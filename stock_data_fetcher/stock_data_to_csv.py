#!/usr/bin/env python3

import csv
<<<<<<< HEAD
from multiprocessing import cpu_count
from multiprocessing.pool import ThreadPool, Pool
=======
from multiprocessing.pool import ThreadPool
>>>>>>> e3719ddb6bb1a2e83e096f8283a180b0131a1811

from stock_data_fetcher import FinanzenDotNet as FDT


CSV_FILE = "aktien_portfolio.csv"


<<<<<<< HEAD
def fetchAndFormatStockData(stock_name):
=======
def stockNameToCsvRows(stock_name):
>>>>>>> e3719ddb6bb1a2e83e096f8283a180b0131a1811
    if not stock_name.strip():
        return None

    print(f"Fetching stock '{stock_name}'")
    stock = FDT.fetchStockCredentials(stock_name)
    if stock is None:
        print(f"'> {stock_name}' Not found")
        return [[stock_name] + [""] * 21]

    stock_data = FDT.fetchStockData(stock["resource"])
    stock_data = FDT.groupStockDataByData(stock_data)

    first_half_filled = [stock["name"], stock["symbol"], stock["wkn"], stock["isin"]] + [""]*6
    first_halb_empty = [""] * len(first_half_filled)

    data_keys = ["years", "share_value", "dividend", "dividend_yield", "revenue_per_share", 
                "price_profit_ratio", "total_revenue", "total_revenue_growth",  "equity",
                "equity_ratio", "dept_ratio", "number_of_employees"]
    rows = [stock_data[data_key] for data_key in data_keys]
    rows = FDT.transposeMatrix(rows)

    rows[0] = first_half_filled + list(rows[0])
    for i, row in enumerate(rows[1:], start=1):
        rows[i] = first_halb_empty + list(row)

    print(f"'> {stock_name}' added")
    return rows
         

<<<<<<< HEAD
def collectStockNames(csv_file):
    stock_names = []
    csv_rows = []
    with open(csv_file, "r", newline='') as file:
        reader = csv.reader(file, delimiter=",")
        for i, row in enumerate(reader):
            if i < 2 or any(row[1:]):
                csv_rows.append(row)
                continue
            if row[0]:
                stock_names.append(row[0])
    return stock_names, csv_rows


def collectStockData(stock_names):
    stock_data = []
    with Pool(cpu_count()) as pool:
        for row in pool.map(fetchAndFormatStockData, stock_names):
            if row is not None:
                stock_data += row
    return stock_data

def writeStockData(csv_data, csv_file):
=======
if __name__ == "__main__":
    
    print("Reading in and fetching stocks...")
    print()

    csv_data = []
    stock_names = []
  
    with open(CSV_FILE, "r", newline='') as file:
        reader = csv.reader(file, delimiter=",")
        for i, row in enumerate(reader):
            if i < 2 or any(row[1:]):
                csv_data.append(row)
                continue
            if row[0]:
                stock_names.append(row[0])
            if i == 4:
                break
            #stock_rows = stockNameToCsvRows(row[0])
            #if not stock_rows:
            #    continue
            #csv_data += stock_rows

    with ThreadPool(20) as pool:
        csv_data += [res for res in pool.map(stockNameToCsvRows, stock_names) if res is not None]

    print()
    print("Writing stock data to CSV...")
>>>>>>> e3719ddb6bb1a2e83e096f8283a180b0131a1811
    with open("filled_"+CSV_FILE, "w", newline='') as file:
        writer = csv.writer(file, delimiter=",")
        for data_row in csv_data:
            writer.writerow(data_row)
<<<<<<< HEAD


if __name__ == "__main__":
    
    print("Reading in and fetching stocks...")
    print()
    stock_names, csv_rows = collectStockNames(CSV_FILE)
    stock_data = collectStockData(stock_names)
    print("Writing stock data to CSV...")
    writeStockData(csv_rows + stock_data, CSV_FILE)
=======
>>>>>>> e3719ddb6bb1a2e83e096f8283a180b0131a1811
    print("Done")
