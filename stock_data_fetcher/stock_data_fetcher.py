#!/usr/bin/env python3

import os.path
import re
import argparse
import sys
import json
from urllib.request import urlopen
from urllib.parse import urljoin, urlencode, urlparse

from bs4 import BeautifulSoup, SoupStrainer
from tabulate import tabulate


class FinanzenDotNet(object):
    BASE_URL = "https://www.finanzen.net"
    SYMBOL_URL = urljoin(BASE_URL, "suchergebnis.asp")
    DATA_URL = urljoin(BASE_URL, "bilanz_guv")
    TD_HEADER_TO_DATA = {
        "Ergebnis je Aktie (unverwässert, nach Steuern)"    : "share_value",
        "Dividende je Aktie"                                : "dividend",
        "Dividendenrendite Jahresende in %"                 : "dividend_yield",
        "Umsatz je Aktie"                                   : "revenue_per_share",
        "KGV (Jahresendkurs)"                               : "price_profit_ratio",
        "Bruttoergebnis vom Umsatz"                         : "total_revenue",
        "Umsatzveränderung in %"                            : "total_revenue_growth",
        "Eigenkapital"                                      : "equity",
        "Eigenkapitalquote in %"                            : "equity_ratio",
        "Fremdkapitalquote in %"                            : "dept_ratio",
        "Anzahl Mitarbeiter"                                : "number_of_employees"
    }

    @classmethod
    def extractStockCredentialsFromSearchPage(cls, html):
        strainer = SoupStrainer(id="suggestBESearch")
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)

        stock_trs = soup.select("#suggestBESearch table thead ~ tr")
        for tr in stock_trs:
            if all(len(td.contents) > 0 for td in tr.select("td")):
                stock_link = tr.select("td > a")[0]["href"]
                break
        else:
            return None

        full_url = urljoin(cls.BASE_URL, stock_link)
        with urlopen(full_url) as response:
            html = response.read()
            return cls.extractStockCredentialsFromAkitenPage(html)

    @classmethod
    def extractStockCredentialsFromAkitenPage(cls, html):
        html = html[:len(html)//4] # Meta data will surely be within the first quater
        strainer = SoupStrainer("head")
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)
        url = soup.select("meta[property='og:url']")[0]["content"]
        title = soup.select("meta[property='og:title']")[0]["content"]

        stock_resource = urlparse(url).path.rsplit("/", 1)[-1].rsplit("-", 1)[0]
        #Example Title: "Coca-Cola Aktie  (850663,KO,US1912161007)"
        #               "Nestlé Aktie  (A0Q4DC,CH0038863350)"
        re_title = re.match(r"(?P<name>.*) Aktie  \((?P<wkn>.*?),(?:(?P<symbol_or_isin>.*?),)?(?P<isin>.*)\)", title)
        stock_name, stock_wkn, stock_symbol, stock_isin = re_title.groups()

        return {
            "name": stock_name,
            "symbol": stock_symbol,
            "isin": stock_isin,
            "wkn": stock_wkn,
            "resource": stock_resource
        }

    @classmethod
    def fetchStockCredentials(cls, name):
        data = urlencode({"_search": name})
        full_url = cls.SYMBOL_URL + "?" + data
        with urlopen(full_url) as response:
            parsed_url = urlparse(response.geturl())
            html = response.read()
            if parsed_url.path == "/suchergebnis.asp":
                return cls.extractStockCredentialsFromSearchPage(html)
            return cls.extractStockCredentialsFromAkitenPage(html)

    
    @classmethod
    def fetchStockData(cls, resource):
        full_url = os.path.join(cls.DATA_URL, resource)
        with urlopen(full_url) as response:
            html = response.read()

        strainer = SoupStrainer(id="bguvform")
        soup = BeautifulSoup(html, "lxml", parse_only=strainer)
        td_headers = soup.select("table > tr > td.font-bold")

        def extractRowWithHeader(header):
            for td_header in td_headers:
                if len(td_header.contents) != 1:
                    continue
                if td_header.text == header:
                    return [td_value.text for td_value in td_header.find_next_siblings("td")]


        th_years = soup.select("#bguvform > div > div:first-child table thead > tr > th")[2:]        
        years = [th_year.text for th_year in th_years]

        data = {
            "years": list(reversed(years))
        }
        for header, data_key in cls.TD_HEADER_TO_DATA.items():
            data[data_key] = list(reversed(extractRowWithHeader(header)))

        return data

    
    @classmethod
    def sortStockData(cls, data, sort_by, ascending=True):
        return data

    @classmethod
    def groupStockDataByYears(cls, data):
        if "data" in data:
            return data

        years = data.pop("years")
        data_keys = list(data.keys())
        data = cls.transposeMatrix(list(data.values()))
        data = dict(zip(years, data))
        return {"data" : data_keys, **data}

    @classmethod
    def groupStockDataByData(cls, data):
        if "years" in data:
            return data

        data_keys = data.pop("data")
        year_keys = list(data.keys())
        data = cls.transposeMatrix(list(data.values()))
        data = dict(zip(data_keys, data))
        return {"years" : year_keys, **data}

    @staticmethod
    def transposeMatrix(matrix):
        return [*zip(*matrix)]


def outputStockDataAsTable(stock, data):
    title = stock["name"]
    sub_title = "WKN: %s\t ISIN: %s\t Symbol: %s" %(stock["wkn"], stock["isin"], stock["symbol"])
    sep = "=" * len(sub_title)

    print()
    print(" " * int(len(sep)/2. - len(title)/2.), title)
    print(sep)
    print(sub_title)
    print(sep)
    data = {key.replace("_", " ").title() : v for key, v in data.items()}
    table = tabulate(data, headers="keys")    
    print(table)
    print()


def outputStockDataAsJson(stock, data):
    print( json.dumps({**stock, **data}) )


def inputLoop():
    inp = ""
    while not inp:
        inp = input("Stock Name: ").strip()
        if inp:
            stock = FinanzenDotNet.fetchStockCredentials(name=inp)
            if stock is not None:
                print("Guessing stock '%s'" %stock["name"])
                data = FinanzenDotNet.fetchStockData(resource=stock["resource"])
                yield (stock, data)
            else:
                print("No stock found. Try again")
                print()
        inp = ""



def parseArgs():
    parser = argparse.ArgumentParser("Fetch stock data online.")
    parser.add_argument("stock", nargs="?", type=str, help="Stock name, WKN or ISIN")

    format_keys = list(FORMATS.keys())
    parser.add_argument("-fmt", "--format", type=str, choices=format_keys, default=format_keys[0], help="Output format")

    sort_keys = list(FinanzenDotNet.TD_HEADER_TO_DATA.values()) + ["data_key"]
    parser.add_argument("-sb", "--sortby", type=str, choices=sort_keys, default="data_key", help="Sort data by data field")
    parser.add_argument("-sa", "--sortasc", action="store_true", help="Sort ascending")
    parser.add_argument("-sd", "--sortdes", action="store_true", help="Sort descending")

    group_keys = list(GROUP_BY_KEYS.keys())
    parser.add_argument("-gb", "--groupby", type=str, choices=group_keys, default=group_keys[0], help="Group data by data key")

    return parser.parse_args()


def processStock(stock, data, args):
    data = GROUP_BY_KEYS[args.groupby](data)
    data = FinanzenDotNet.sortStockData(data, args.sortby, ascending=((args.sortasc==True) or not (args.sortdes==True)))
    FORMATS[args.format](stock, data)
    

FORMATS = {
    "table" : outputStockDataAsTable,
    "json"  : outputStockDataAsJson
}

GROUP_BY_KEYS = {
    "year"  : FinanzenDotNet.groupStockDataByYears,
    "data"  : FinanzenDotNet.groupStockDataByData
}

if __name__ == "__main__":
    args = parseArgs()

    while args.stock is None:
        stock, data = inputLoop()
        processStock(stock, data, args)
    
    stock = FinanzenDotNet.fetchStockCredentials(name=args.stock)
    if stock is None:
        print("No stock found")
    else:
        data = FinanzenDotNet.fetchStockData(resource=stock["resource"])
        processStock(stock, data, args)
        



    
