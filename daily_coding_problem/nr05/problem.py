


def cons(a, b):
    """ Constructs pair """
    def pair(f):
        return f(a, b)
    return pair


def car(pair):
    """ Returns first element of pair """
    return pair(lambda a,b: a)


def cdr(pair):
    """ Returns first element of pair """
    return pair(lambda a,b: b)



pair = cons(3, 4)
assert car(pair) == 3
assert cdr(pair) == 4



