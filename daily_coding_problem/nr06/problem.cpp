#include <iostream>
#include <cstdint>
#include <stdexcept>


class XORNode{
    public:
        XORNode(int element):element(element),both(0){}

        int element;
        std::uintptr_t both;
};

class XORList{
    public:
        XORList():length(0), start(nullptr), end(nullptr){}

        void add(int element){
            length++;

            if(start == nullptr){
                start = new XORNode(element);
                std::uintptr_t start_int = reinterpret_cast<std::uintptr_t>(start);
                end = start;
                return;
            }

            XORNode* prev = end;
            end = new XORNode(element);
            std::uintptr_t end_int = reinterpret_cast<std::uintptr_t>(end);
            std::uintptr_t prev_int = reinterpret_cast<std::uintptr_t>(prev);
            end->both = prev_int;
            prev->both ^= end_int;
        }
        
        int get(int index){
            if(length < index)
                throw std::out_of_range("XORList index out of range");

            if(index == 0)
                return start->element;

            XORNode* last = start;
            XORNode* cur = reinterpret_cast<XORNode*>(start->both);
            std::uintptr_t next;
            for(unsigned int i=1; i<index; ++i){
                next = cur->both ^ reinterpret_cast<std::uintptr_t>(last);
                last = cur;
                cur = reinterpret_cast<XORNode*>(next);
            }
            
            return cur->element;
        }

        unsigned int length;

    protected:
        XORNode* start;
        XORNode* end;
};


int main(){
    XORList l1;

    for(short i=0; i<1492835; ++i){
        l1.add(i*2);
    }

    std::cout << "l1:";
    for(short i=0; i<l1.length; ++i){
        std::cout << l1.get(i) <<",";
    }
    std::cout << std::endl;

    return 0;
}
