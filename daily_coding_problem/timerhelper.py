from timeit import timeit
from functools import wraps

# Timing helper
def wrapper(func, *args, **kwargs):
    @wraps(func)
    def wrapped():
        return func(*args, **kwargs)
    return wrapped


def timeFuncs(func_list, arg_list, number=1):
    if arg_list:
        func_list = [wrapper(func, *arg_list) for func in func_list]

    for func in func_list:
        print(func.__name__, ": ", timeit(wrapper(func, ), number=number)*1000)
