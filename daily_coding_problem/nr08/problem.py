import sys, os
sys.path.append(os.path.abspath(".."))
from timerhelper import timeFuncs


class Node(object):
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right


def getUnivalSubtreesRec(node):
    l, r = node.left, node.right
    
    # Node is leave, so trivially a unival subtree
    if not (l or r):
        return 1
    
    st_count = 0
    is_univalsub = True
    
    if r:
        count = getUnivalSubtreesRec(r)
        # Is right subtree unival with the same value as parent
        is_univalsub = is_univalsub and (count != 0) and (r.value == node.value)
        st_count += count
        
    if l:
        count = getUnivalSubtreesRec(l)
        is_univalsub = is_univalsub and (count != 0) and (l.value == node.value)
        st_count += count

    # Existing subtrees are unival with same value as parent
    if is_univalsub:
        st_count += 1
        
    return st_count


def getUnivalSubtreesIter(node):
    stack = [(node, [])] # Stack storing (<node>,
                        # <list of parent nodes qualifying for unival subtree>)
    nodes = {} # Store for each node if it still qualifies for unival subtree
                # using <id(node)>:<bool>

    while stack:
        cnode, parents = stack.pop()
        l, r = cnode.left, cnode.right

        l_val = r_val = cnode.value 
        if l:
            l_val = l.value 
        if r:
            r_val = r.value
        # Values of parent and existing child nodes differ
        if not (l_val == r_val == cnode.value):
            # All parents reaching back cannot longer be unival subtree
            for parent in parents:
                nodes[id(parent)] = False
            # Neither can the current node
            nodes[id(cnode)] = False
            parents = []
        # Values are equal, so still unival subtree
        else:
            # Mark current node as unival subtree and add to parent list
            # for the following children
            nodes[id(cnode)] = True
            parents.append(cnode)
                        
        if l:
            stack.append((l, parents))
        if r:
            stack.append((r, parents))

    # Amount of True-valued nodes equals number of unival subtrees
    return sum(nodes.values())
        

root1 = Node(0, Node(1), Node(0, Node(1, Node(1), Node(1)), Node(0)))
assert getUnivalSubtreesRec(root1) == 5
assert getUnivalSubtreesIter(root1) == 5

root2 = Node(0)
assert getUnivalSubtreesRec(root2) == 1
assert getUnivalSubtreesIter(root2) == 1

root3 = Node(0, Node(0, Node(0), Node(0)), Node(0, Node(0), Node(0)))
assert getUnivalSubtreesRec(root3) == 7
assert getUnivalSubtreesIter(root3) == 7


def createTree(size):
    root = Node(0)
    stack =  [root]
    while stack and len(stack) <= 100:
        n = stack.pop()
        l = Node(0)
        r = Node(1)
        n.left = l
        n.right = r
        stack.append(l)
        stack.append(r)
    return root

root4 = createTree(400)
def testRec():
    getUnivalSubtreesRec(root4)
def testIter():
    getUnivalSubtreesIter(root4)
    
timeFuncs([testRec, testIter], [], number=1000)

"""
Timing Results:
    Recurtion method is about 2 times faster than iteration.
    My iteration method has lots of (probably unneeded) overhead, which
    is likely the reason for the poor performance)

"""




