
# Fibonacci sequence with ignoring the step if two consequtive
# characters of the message for an integer above 26
def decode(msg):
    a, b = 1, 1

    if len(msg) == 0:
        return 0
    
    for i, char in enumerate(msg[1:], start=1):
        if int(msg[i-1] + char) > 26:
            continue
        a, b = b, a+b

    return b        


assert decode("") == 0
assert decode("1") == 1
assert decode("66") == 1
assert decode("123") == 3
assert decode("1234") == 3
assert decode("12121") == 8

