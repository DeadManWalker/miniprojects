import json
import sys, os
sys.path.append(os.path.abspath(".."))
from timerhelper import timeFuncs


class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# Recursive serializations
def serializeRec(node):
    if node is None:
        return None

    return json.dumps({
        "val" : node.val,
        "left" : serializeRec(node.left),
        "right" : serializeRec(node.right)
    })

# Recursive deserializations
def deserializeRec(node_str):
    if node_str == None:
        return None
    
    jnode = json.loads(node_str)
    node = Node(jnode["val"], deserializeRec(jnode["left"]),
                deserializeRec(jnode["right"]))
    return node



# Iterative serializations
def serializeIter(node):
    nodes = {}
    node_stack = [("root", node)]

    while node_stack:
        nid, cnode = node_stack.pop()
        ndict = {"val" : cnode.val}
        l = cnode.left
        r = cnode.right
        
        if l:
            lid = id(l)
            node_stack.append((lid, l))
            ndict["left"] = lid
        else:
            ndict["left"] = None
            
        if r:
            rid = id(r)
            node_stack.append((rid, r))
            ndict["right"] = rid
        else:
            ndict["right"] = None

        nodes[nid] = ndict

    return json.dumps(nodes)
        
        
# Iterative deserializations
def deserializeIter(node_str):
    nodes = json.loads(node_str)
    
    ndict = nodes["root"]
    root = Node(ndict["val"])
    node_stack = [(root, ndict)]

    while node_stack:
        cnode, ndict = node_stack.pop()
        lid = ndict["left"]
        rid = ndict["right"]

        if lid:
            ldict = nodes[str(lid)]
            lnode = Node(ldict["val"])
            cnode.left = lnode
            node_stack.append((lnode, ldict))
        if rid:
            rdict = nodes[str(rid)]
            rnode = Node(rdict["val"])
            cnode.right = rnode
            node_stack.append((rnode, rdict))

    return root
    



node = Node('root', Node('left', Node('left.left')), Node('right'))
assert deserializeRec(serializeRec(node)).left.left.val == 'left.left'
assert deserializeIter(serializeIter(node)).left.left.val == 'left.left'

def testRec():
    deserializeRec(serializeRec(node))
def testIter():
    deserializeIter(serializeIter(node))
timeFuncs([testRec, testIter], [], number=1000)


"""
Timing Results:
    Iteration method is about 1.5 times faster than recursion

"""




