from itertools import chain
from random import randint

import sys, os
sys.path.append(os.path.abspath(".."))
from timerhelper import timeFuncs


# Straight forward using division
# Complexity: O(2n)
# Memory: O(n)
def indexedProduct(num_array):
    product = 1
    for num in num_array:
        product *= num

    prod_array = []
    for num in num_array:
        prod_array.append(product // num)

    return prod_array

# Multiplying for every element every time separately
# Complexity: O(n*(n-1)) ~ O(n^2)
# Memory: O(n)
def indexedProductNoDiv1(num_array):
    array_len = len(num_array)
    prod_array = [1]*array_len
    for num_i, num in enumerate(num_array):
        for prod_i in chain(range(num_i), range(num_i+1, array_len)):
            prod_array[prod_i] *= num

    return prod_array

# Multiplying to and until every element and multiplying those products in a third run
# Complexity: O(3n)
# Memory: O(2n)
def indexedProductNoDiv2(num_array):
    array_len = len(num_array)
    prod_array_before = []
    prod_array_after = []
    
    product = 1
    for num in num_array:
        prod_array_before.append(product)
        product *= num

    product = 1
    for num in reversed(num_array):
        prod_array_after.append(product)
        product *= num

    for i, (prod_before, prod_after) in \
     enumerate(zip(prod_array_before, reversed(prod_array_after))):
        prod_array_before[i] = prod_before*prod_after

    return prod_array_before


a = [1,2,3,4,5]
for func in [indexedProduct, indexedProductNoDiv1, indexedProductNoDiv2]:
    print(func(a))


N = 170
input_array = [randint(1, 500) for _ in range(N)]
timeFuncs([indexedProduct, indexedProductNoDiv1, indexedProductNoDiv2], [input_array], number=10)

"""
Timing results best-worst:
    2. indexedProduct
    1. indexedProductNoDiv2
    3. indexedProductNoDiv1

indexProductNoDiv2 only slightly faster than indexProduct between around 60 and 170
"""
