

# My attempt not seeing the fibonacci sequence
def getUniqueWaysOneTwo(n_steps):
    if n_steps == 1:
        return {(1,)}
    comps = getUniqueWays(n_steps-1)
    new_comps = set()
    for t in comps:
        t1 = (1,) + t
        t2 = t + (1,)
        new_comps.add(t1)
        new_comps.add(t2)
        if t[0] == 1:
            new_comps.add((2,) + t[1:])
        if t[-1] == 1:
            new_comps.add(t[:-1] + (2,))
    return new_comps


# Solution's attemp seeing the fibonaccy sequence
# Complexity: O(n)
# Space: O(1)
def staircase(n):
    a, b = 1, 2
    for _ in range(n - 1):
        a, b = b, a + b
    return a


# Complexity: O(n*|X|)
# Space: O(n)
def staircaseX(n, X):
    cache = [0 for _ in range(n + 1)]
    cache[0] = 1
    for i in range(1, n + 1):
        cache[i] += sum(cache[i - x] for x in X if i - x >= 0)
    return cache[n]

print(staircaseX(8, [2,3,4]))
