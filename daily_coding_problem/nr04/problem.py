import sys, os
sys.path.append(os.path.abspath(".."))
from timerhelper import timeFuncs



def findFirstMissingPositive(num_array):
    # Separate negative (<1) from positive by moving
    # negatives to the front
    neg_i = 0
    for i, num in enumerate(num_array):
        if num < 1:
            num_array[neg_i], num_array[i] = num_array[i], num_array[neg_i]
            neg_i += 1

    # Ignore the negatives and start going through the positives.
    # Use each element as and index and annotate the element at
    # that index (if within bounds) with a negative sign
    bound = len(num_array) - neg_i
    for i, num in enumerate(num_array[neg_i:]):
        if num < bound:
            num_array[num + neg_i - 1] = -abs(num_array[num + neg_i - 1])

    # Of the former positives, find the first one that is still
    # positive. Its index corresponds to the lowest missing
    # positive number
    for i, num in enumerate(num_array[neg_i:]):
        if num > 0:
            return i + 1 


input_array = [6,-2,8,4,0,7,10,-44,3,33,1,2,2] # 5 is missing
missing = findFirstMissingPositive(input_array)
print("%s is missing %s" %(input_array, missing))




