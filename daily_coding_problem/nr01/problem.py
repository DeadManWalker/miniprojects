from random import randint

import sys, os
sys.path.append(os.path.abspath(".."))
from timerhelper import timeFuncs


# Using dictionary
# Complexity: Best O(n), Worst O(n!)
# Memory: 0(n)
def dict_addsUpTo(num_array, sum):
    used = {}
    for num2 in num_array:
        num1 = sum-num2
        if num1 in used:
            return (num1, num2)
            break
        used[num2] = True

# Using list
# Complexity: O(n!)
# Memory: 0(n)
def list_addsUpTo(num_array, sum):
    used = []
    for num2 in num_array:
        num1 = sum-num2
        if num1 in used:
            return (num1, num2)
            break
        used.append(num2)

# Using set
# Complexity: Best O(n), Worst O(n!)
# Memory: 0(n)
def set_addsUpTo(num_array, sum):
    used = set()
    for num2 in num_array:
        num1 = sum-num2
        if num1 in used:
            return (num1, num2)
            break
        used.add(num2)



N = 100000
input_arr = [randint(-10000, 10000) for _ in range(N)]
input_sum = 1123

timeFuncs([dict_addsUpTo, list_addsUpTo, set_addsUpTo], [input_arr, input_sum], number=1000)

"""
Timing results best-worst:
    1. dict_addsUpTo
    2. set_addsUpTo
    3. list_addsUpTo

Dictionary and set have some overhead, so for very small N around 5, the timeit results are reversed.
Dict is always slightly faster than set.
"""
