#!/bin/python3

import requests
import sys
import re
import json
import argparse

from random import shuffle

from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser


YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"
#input your own api key here
YOUTUBE_API_KEY = "AIzaSyBsAHOeEYbjMsx4LL2l9pFLIxOXXOALdK0"

def getPlaylistVidoes(_playlistID):
	videos = []
	#load youtube api 
	youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, developerKey=YOUTUBE_API_KEY)

	ytreq = youtube.playlistItems().list(
		part="contentDetails",
		playlistId=_playlistID,
		maxResults=50
	)
	query = ytreq.execute()
	for item in query.get("items", []):
		videos.append(item["contentDetails"]["videoId"])
	nextpage = youtube.playlistItems().list_next(ytreq, query)
	while(nextpage!=None): 
		query = nextpage.execute()
		for item in query.get("items", []):
			videos.append(item["contentDetails"]["videoId"])
		nextpage = youtube.playlistItems().list_next(ytreq, query)
	return videos

def injectVideos(tg_url,videos, randomize=False, start=None, end=None):
	res = requests.get(tg_url)
	session = res.cookies
	headers = {
		'content-type': "application/json;charset=utf-8"
	}
	youtubeVideoWrapper = {
		'mediaId': "",
		'mediaServiceId': "youtube",
	}
	api_temp = "https://togethertube.com/api/v1/rooms/ROOM/playlist/votes"
	room = re.search('(?<=rooms/).*', tg_url).group(0)
	api_url = api_temp.replace("ROOM", room)

	if(start == None):
		start = 0
	if(end == None):
		end = len(videos)
	indexes = list(range(start, end))
	if randomize:
		shuffle(indexes)
	cnt = 0
	for i in indexes:
		youtubeVideoWrapper["mediaId"] = videos[i]
		add = requests.post(api_url, headers=headers, data=json.dumps(youtubeVideoWrapper), cookies=session)
		if(add.status_code==201):
			cnt += 1
	return cnt;

def getPlaylistID(url):
	return re.search('(?<=list=).*', url).group(0)


def main():
	parser = argparse.ArgumentParser(description="Program for adding Youtubeplaylist to Togethertube session")

	parser.add_argument('url', action='store', help='togethertube room url')
	parser.add_argument('-shuffle', action='store_true', dest='shuffle', default=False, help='randomize queue')
	parser.add_argument('playlists', metavar='playlist', nargs='+', help='youtube playlist url')
	parser.add_argument('-start', action='store', dest='start', default=None, type=int, help='specify start postion')
	parser.add_argument('-end', action='store', dest='end', default=None, type=int, help='specify end postion')

	parameters = parser.parse_args()
	print("Started may take some time")
	playlistids = []
	for plist in parameters.playlists:
		playlistids.append(getPlaylistID(plist))
	videos = []
	for id in playlistids:
		videos += getPlaylistVidoes(id)
	cnt = injectVideos(parameters.url, videos, parameters.shuffle, parameters.start, parameters.end)
	print("Added", cnt, "videos successfully")


if __name__ == "__main__":
	main()
