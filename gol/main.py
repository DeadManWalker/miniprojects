import sys
from classes.MainWindow import MainWindow

from PyQt4.QtGui import QApplication


def main():
    app = QApplication(sys.argv)
    gol = MainWindow(cell_count=(80,80))
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
else:
    print("This is not a module!")
