from PyQt4.QtCore import *
from PyQt4.QtGui import *


class CentralWidget(QWidget):
    def __init__(self, parent):
        super(CentralWidget, self).__init__(parent)

        self.painter = QPainter()
        self.painter.setPen(Qt.black)
        self.cells = []
        self.parent_ = parent
       

        self.setStyleSheet("color: black;")
        self.pal = QPalette(Qt.green)
        self.setAutoFillBackground(True)
        self.setPalette(self.pal)        

    def paintEvent(self, event):
        self.painter.begin(self)
        for cell in self.cells:
            self.painter.drawRect(cell)
            self.painter.fillRect(cell, cell.getColor())
        self.painter.end()

    def mousePressEvent(self, event):
        self.parent_.drawCell(event)        
        
    def mouseMoveEvent(self, event):
        self.parent_.drawCell(event) 
