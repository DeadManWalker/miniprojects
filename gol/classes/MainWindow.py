import random

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from classes.CentralWidget import *
from classes.Cell import *
from classes.Toolbar import *


class MainWindow(QMainWindow):
    
    def __init__(self, cell_count=(150,150), delay=80, parent=None):
        super(MainWindow, self).__init__(parent)

        self.CELL_COUNT_t = cell_count
        self.CELL_COUNT_X, self.CELL_COUNT_Y = self.CELL_COUNT_t
        self.CELL_COUNT = self.CELL_COUNT_X * self.CELL_COUNT_Y
        self.delay = delay
        self.RESIZE_TIMEOUT = 200
        self.run = False
        self.startup = []
        self.draw_cells = False
        self.draw_object = {}

        self.setWindowTitle("Game Of Life")

        self.main = CentralWidget(self)
        self.setCentralWidget(self.main)

        self.toolbar = Toolbar("Toolbar", self)
        self.addToolBar(Qt.LeftToolBarArea, self.toolbar)
        
        self.timer = QTimer(self)
        self.resize_timer = QTimer(self)
        self.resize_timer.timeout.connect(self.resizeTimeout)
        self.showMaximized()
        self.CELL_WIDTH, self.CELL_HEIGHT = self.calcCellSize()
        
        self._buildCells()

    def resizeEvent(self, event):
        self.resize_timer.stop()
        self.resize_timer.start(self.RESIZE_TIMEOUT)

    def resizeTimeout(self):
        self.resize_timer.stop()
        self.CELL_WIDTH, self.CELL_HEIGHT = self.calcCellSize()
        self.rebuildCells()

    def _buildCells(self):
        new_coords = [0,0]
        for i in range(self.CELL_COUNT):
            if i%self.CELL_COUNT_X == 0 and i != 0:
                new_coords[0] = 0
                new_coords[1] += self.CELL_HEIGHT + 1
                
            cell = Cell(new_coords[0], new_coords[1], self.CELL_WIDTH, self.CELL_HEIGHT, i, self.CELL_COUNT_t)
            cell.cell_index = i
            self.main.cells.append(cell)
            new_coords[0] += self.CELL_WIDTH + 1
        self.main.update()

    def rebuildCells(self):
        new_coords = [0,0]
        for i in range(self.CELL_COUNT):
            cell = self.main.cells[i]
            if i%self.CELL_COUNT_X == 0 and i != 0:
                new_coords[0] = 0
                new_coords[1] += self.CELL_HEIGHT + 1

            cell.setRect(new_coords[0], new_coords[1], self.CELL_WIDTH, self.CELL_HEIGHT)
            new_coords[0] += self.CELL_WIDTH + 1
        self.main.update()
       

    def calcCellSize(self):
        return (int(self.main.width() / self.CELL_COUNT_X) , int(self.main.height() / self.CELL_COUNT_Y))

    def loadCoords(self, cell):
        try:
            for x,y in self.draw_object["coords"]:
                self.main.cells[(y*self.CELL_COUNT_X + x) + cell].setLifeState(CELL_ALIVE)
            self.main.update()
        except IndexError:
            pass

    def reloadStartup(self):
        self.clearCells()
        for i in self.startup:
            self.main.cells[i].setLifeState(CELL_ALIVE)
        self.main.update()
            
    def getAliveNeighborIndices(self, cell):
        alive = []
        for i in cell.NEIGHBOR_INDICES:
            if self.main.cells[i].getLifeState():
                alive.append(i)
        return alive

    def runGame(self):
        if not self.run:
            self.run = True
            self.timer.setInterval(self.delay)
            self.timer.setSingleShot(False)
            self.timer.timeout.connect(self.runGame)
            self.timer.start(self.delay)
            return
            
        for i in range(self.CELL_COUNT):
            life = self.main.cells[i].getLifeState()
            neighbor_count = len(self.getAliveNeighborIndices(self.main.cells[i]))
            
            if not life and neighbor_count == 3:
                self.main.cells[i].setLifeStateNext(CELL_ALIVE)
                continue
            if neighbor_count < 2 and life:
                self.main.cells[i].setLifeStateNext(CELL_DEAD)
                continue
            if neighbor_count in (2,3) and life:
                self.main.cells[i].setLifeStateNext(CELL_ALIVE)
                continue
            if life and neighbor_count > 3:
                self.main.cells[i].setLifeStateNext(CELL_DEAD)

        for i in range(self.CELL_COUNT):
            self.main.cells[i].setLifeState(self.main.cells[i].getLifeStateNext())
            
        self.main.update()

    def stopGame(self):
        self.timer.stop()
        self.run = False

    def clearCells(self):
        for i in range(self.CELL_COUNT):
            self.main.cells[i].setLifeState(CELL_DEAD)
            self.main.cells[i].setLifeStateNext(CELL_DEAD)
        self.main.update()

    def loadRandom(self, chance=0.5):
        self.startup[:]
        for i in range(self.CELL_COUNT):
            if not random.random() <= chance:
                continue
            self.main.cells[i].setLifeState(CELL_ALIVE)
            self.startup.append(i)
        self.main.update()

    def drawCell(self, event):
        if not (self.draw_cells or (self.draw_object and isinstance(event, QMouseEvent))):
            return
        
        x = int(event.x() / (self.CELL_WIDTH+1))
        y = int(event.y() / (self.CELL_HEIGHT+1))
        cell = y*self.CELL_COUNT_X + x
        
        if self.draw_cells:
            try:
                self.main.cells[cell].setLifeState(CELL_ALIVE)
                self.main.update()
            except IndexError:
                pass
            
        elif self.draw_object and isinstance(event, QMouseEvent):
            self.loadCoords(cell)
        
        
            

