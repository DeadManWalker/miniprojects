import json
import glob
import os

from PyQt4.QtCore import *
from PyQt4.QtGui import *


JSON_PATH = "./json"


class Toolbar(QToolBar):
    
    def __init__(self, text, parent):
        super(Toolbar, self).__init__(text, parent)

        self.JSON = {}
        self._loadJson()
        self.parent_ = parent

        self.insert_buttons = []
        group_layout = QVBoxLayout()
        group = QGroupBox("Insert")
        group.setLayout(group_layout)
        for categ in sorted(self.JSON):
            l = QLabel(categ)
            group_layout.addWidget(l)
            for info in self.JSON[categ]:
                b = QPushButton(info["name"])
                b.value = info
                b.setCheckable(True)
                b.pressed.connect(self.loadFromJson)
                self.insert_buttons.append(b)
                group_layout.addWidget(b)
        self.addWidget(group)
        
        self.addSeparator()
        #self.insertSpacer()
        button = QPushButton("Load Random")
        button.pressed.connect(self.loadRandom)
        self.addWidget(button)

        button = QPushButton("Reload Startup")
        button.pressed.connect(self.reloadStartup)
        self.addWidget(button)

        button = QPushButton("Draw Cells")
        button.setCheckable(True)
        button.toggled.connect(self.drawCells)
        self.addWidget(button)
        
        self.addSeparator()
        #self.insertSpacer()
        
        button = QPushButton("Run")
        button.setCheckable(True)
        button.toggled.connect(self.toggleRun)
        self.addWidget(button)

        button = QPushButton("Clear Cells")
        button.pressed.connect(self.clearCells)
        self.addWidget(button)

    def _loadJson(self):
        for file in glob.glob(os.path.join(JSON_PATH, "*.json")):
            with open(file) as f:
                to_json = json.load(f)
                if to_json["category"] in self.JSON:
                    self.JSON[to_json["category"]].append(to_json)
                else:
                    self.JSON[to_json["category"]] = [to_json]
                
    def toggleRun(self, run):
        source = self.sender()
        
        if run:
            source.setText("Pause")
            self.parent_.runGame()
        else:
            source.setText("Run")
            self.parent_.stopGame()

    def loadFromJson(self):
        value = self.sender().value
        for button in self.insert_buttons:
            if button.text() == value["name"]:
                continue
            button.setChecked(False)
        self.parent_.draw_object = value

    def loadRandom(self):
        self.parent_.loadRandom()

    def reloadStartup(self):
        self.parent_.reloadStartup()

    def clearCells(self):
        self.parent_.clearCells()

    def drawCells(self, pressed):
        self.parent_.draw_cells = pressed

        
            
        

