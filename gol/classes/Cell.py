from PyQt4.QtCore import *
from PyQt4.QtGui import *


CELL_ALIVE = True
CELL_DEAD = False


class Cell(QRect):
    
    def __init__(self, top, left, width, height, index, cell_count):
        super(Cell, self).__init__(top, left, width, height)
        self.is_alive = False
        self.is_alive_next = False
        self.color = Qt.white
        self.CELL_COUNT = cell_count
        self.INDEX = index
        self.NEIGHBOR_INDICES = self.getNeighborIndices()
        

    def setLifeState(self, bool_):
        if bool_:
            self.is_alive = True
            self.color = Qt.black
        else:
            self.is_alive = False
            self.color = Qt.white

    def setLifeStateNext(self, bool_):
        if bool_:
            self.is_alive_next = True
        else:
            self.is_alive_next = False

    def getLifeState(self):
        return self.is_alive

    def getLifeStateNext(self):
        return self.is_alive_next

    def getColor(self):
        return self.color

    def getNeighborIndices(self, cell_count=None):
        if cell_count == None:
            cell_count = self.CELL_COUNT
            
        UP = self.INDEX - self.CELL_COUNT[0]
        UP_LEFT = UP - 1
        UP_RIGHT = UP + 1
        DOWN = self.INDEX + self.CELL_COUNT[0]
        DOWN_LEFT = DOWN - 1
        DOWN_RIGHT = DOWN + 1
        LEFT = self.INDEX - 1
        RIGHT = self.INDEX + 1

        sides = {
            "UP": UP,
            "UP_LEFT": UP_LEFT,
            "UP_RIGHT": UP_RIGHT,
            "DOWN": DOWN,
            "DOWN_LEFT": DOWN_LEFT,
            "DOWN_RIGHT": DOWN_RIGHT,
            "LEFT": LEFT,
            "RIGHT": RIGHT
        }
            
        if self.INDEX < self.CELL_COUNT[0]:
            sides.pop("UP", None)
            sides.pop("UP_LEFT", None)
            sides.pop("UP_RIGHT", None)
        if self.INDEX >= self.CELL_COUNT[0] * (self.CELL_COUNT[1] - 1):
            sides.pop("DOWN", None)
            sides.pop("DOWN_LEFT", None)
            sides.pop("DOWN_RIGHT", None)
        if self.INDEX % self.CELL_COUNT[0] == 0:
            sides.pop("UP_LEFT", None)
            sides.pop("LEFT", None)
            sides.pop("DOWN_LEFT", None)
        if (self.INDEX+1) % self.CELL_COUNT[0] == 0:
            sides.pop("UP_RIGHT", None)
            sides.pop("RIGHT", None)
            sides.pop("DOWN_RIGHT", None)
        return set(sides.values())

        
        
        
            
        

