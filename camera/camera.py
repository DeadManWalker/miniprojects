#!/usr/bin/env python2

"""
Record with onboard webcam for passed or default seconds
and save output
"""

import numpy as np
import cv2
import time, datetime
import os, sys


# Default recoding time in seconds
RECORD_TIME = 15 
# Video output path
OUTPUT_PATH = os.path.join(os.path.dirname(__file__), "output")


def main():
    try:
        record_time = int(sys.argv[1])
    except (ValueError, IndexError):
        record_time = RECORD_TIME
    date = datetime.datetime.now()
    filename = "%s-%s-%s_%s:%s:%s.avi" %(date.day, date.month, date.year, date.hour, date.minute, date.second)
    start_time = time.time()

    cap = cv2.VideoCapture(0)
    fourcc = cv2.VideoWriter_fourcc(*"MJPG")
    writer = cv2.VideoWriter(os.path.join(OUTPUT_PATH, filename), fourcc, 20.0, (640,480))

    while cap.isOpened() and time.time()-start_time < record_time:
        ret, frame = cap.read()
        if not ret:
            break
        writer.write(frame)

    cap.release()
    writer.release()       


if __name__ == "__main__":
    main()


