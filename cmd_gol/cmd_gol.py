#!/usr/bin/env python3


import time
import sys
import itertools
from random import randint


class BasicCmdDrawer(object):
    def __init__(self, dead_char, living_char):
        self.chars = [dead_char, living_char]

    def draw(self, grid1d, size):
        [print("") for _ in range(100)]
        for index, state in enumerate(grid1d):
            print(self.chars[state], end="")
            if (index+1) % size == 0:
                print()
    

import curses
class CursesDrawer(object):
    def __init__(self, dead_char, living_char):
        self.chars = [dead_char, living_char]
        self.stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        curses.curs_set(False)
        self.stdscr.keypad(True)

    def cleanup(self):
        curses.nocbreak()
        self.stdscr.keypad(False)
        curses.echo()
        curses.endwin()

    def to1D(self, x, y, size):
        return x * size + y 

    def to2D(self, index, size):
        height = index // size
        width = index % size
        return width, height

    def draw(self, grid1d, size):
        try:
            self.stdscr.clear()
            for index, state in enumerate(grid1d):
                x, y = self.to2D(index, size)
                self.stdscr.addch((y, x), self.chars[state])
            self.stdscr.refresh()
        except:
            self.cleanup()

class GOL(object):
    def __init__(self, size, drawer):
        self.drawer = drawer        
        self._size = None
        self._grid = None
        self._next_grid = None
        
        self.resize(size)
       
    @property
    def size(self):
        return self._size

    def resize(self, size):
        self._size = n = int(size)
        self._grid = [randint(0,1) for _ in range(n*n)]
        self._next_grid = self._grid[:]

    def to1D(self, x, y):
        return y * self._size + x 

    def to2D(self, index):
        height = index // self._size
        width = index % self._size
        return width, height

    def neighbors(self, index):
        x, y = self.to2D(index)
        for a, b in itertools.product((0,-1,+1), (0,-1,+1)):
            xa, yb = x+a, y+b
            if self.inBounds(xa, yb) and not (xa == x and yb == y):
                yield self.to1D(xa, yb)
    
    def inBounds(self, x, y):
        return (0 <= x < self._size and 0 <= y < self._size)

    def updateCell(self, index):
        state = self._grid[index]
        nb = self.neighbors(index)
        n_living = sum(self._grid[i] for i in nb)

        if state == 0:
            if n_living == 3:
                self._next_grid[index] = 1
            return

        if n_living < 2 or n_living > 3:
            self._next_grid[index] = 0
            return

    def update(self):
        for index, _ in enumerate(self._grid):
            self.updateCell(index)
        static = (self._grid == self._next_grid)
        self._grid = self._next_grid[:]
        return not static

    def draw(self):
        self.drawer.draw(self._grid, self._size)

def main():
    try:
        size = int(sys.argv[1])
    except (ValueError, IndexError):
        size = 10

    try:
        speed = float(sys.argv[2])
    except (ValueError, IndexError):
        speed = 1

    try:
        steps = int(sys.argv[3])
    except (ValueError, IndexError):
        steps = -1

    drawer = BasicCmdDrawer("- ", "x ")
    gol = GOL(size, drawer)

    while steps != 0:
        if not gol.update():
            break
        gol.draw()
        time.sleep(speed)
        steps -= 1

if __name__ == "__main__":
    main()
