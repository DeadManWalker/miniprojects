#!/usr/bin/python3

"""
Listens for two usb scanner located in /dev/input/by-path/
"""


import struct
import sys
import os, os.path
import time
import datetime
import json
import urllib.parse, urllib.request, urllib.error
from threading import Thread, Event


################################################
### END CONFIG
################################################

# Path where device output is stored
BASE_PATH = "/dev/input/by-path"

# Length of barcode excluding prefix and
# termination character
# !Barcode is expected to have a prefix and a termination character!
CODE_LENGTH = 32

# List barcode char-prefixes to determin the role of
# each scanner and the total number of scanners
# to listen for
CODE_PREFIXES = {
    "6" : "entry",
    "1" : "exit"
}

# Clear code buffer after specified milliseconds.
# <None> for no clearing
CLEAR_TIMEOUT = 50

SERVER_API_KEY = "93f94295hdlwm58dj0923n"
SERVER_BASE_URL = "http://idscreates.com:8080/api/1.0/"
SERVER_ADD_PATH = "access/add/"
SERVER_DATABASE_PATH = "employees/active/"

# Path to json database
DATABASE_PATH = "employees.json"
# Replace local with remote database every x minutes
DATABASE_UPDATE_INTERVAL = 60

################################################
### END CONFIG
################################################



__author__ = "Kevin Hambrecht"
__version__ = "1.0"



class InputReader(object):
    """
    Read device output for given devices and provides dictionary interface to retrieve data.
    """
    
    FORMAT = 'llHHI'
    EVENT_SIZE = struct.calcsize(FORMAT)
    CODE_ASCII_MAP = (None,None,'1','2','3','4','5','6','7','8','9','0',None,None,None,None,
                      'q','w','e','r','t','z','u','i','o','p',None,None,None,None,'a','s',
                      'd','f','g','h','j','k','l',None,None,None,None,None,'y','x','c','v',
                      'b','n','m',None,None,None,None,None,None,None,None,None,None,None,
                      None,None,None,None,None,None,None,None,None,None,None,None,None)

    def __init__(self, path):
        self.file = None
        self.path = path
        self.file = os.open(path, os.O_RDONLY|os.O_NONBLOCK)
        self._struct = {
            "tv_sec" : [],
            "tv_usec" : [],
            "_type" : [],
            "code" : [],
            "value" : [],
            "ascii_code" : []
        }

    def update(self):
        """
        Updates _struct dictionary with latest device input read.
        Clears _struct dictionary after all input has been read.
        """
        event = self._read(True)
        while event is not None:
            try:
                tv_sec, tv_usec, _type, code, value = struct.unpack(InputReader.FORMAT, event)
                self._struct["tv_sec"].append(tv_sec)
                self._struct["tv_usec"].append(tv_usec)
                self._struct["_type"].append(_type)
                self._struct["code"].append(code)
                self._struct["value"].append(value)
                self._struct["ascii_code"].append(InputReader._getAscii(code))
            except struct.error:
                pass
            finally:
                event = self._read()

    @staticmethod
    def _getAscii(code):
        try:
            return InputReader.CODE_ASCII_MAP[code]
        except IndexError:
            return None;

    def _read(self, reset=False):
        try:
            event = os.read(self.file, InputReader.EVENT_SIZE)
            return event if event else None
        except OSError as e:
            # EOF
            if e.errno != 11:
                raise
            if reset:
                self._struct["tv_sec"] = []
                self._struct["tv_usec"] = []
                self._struct["_type"] = []
                self._struct["code"] = []
                self._struct["value"] = []
                self._struct["ascii_code"] = []
            return None

    def __getitem__(self, i):
        return self._struct[i]


    def __del__(self):
        if self.file is not None:
            os.close(self.file)


class Scanner(object):
    """
    Stores device output from given device up to a termination char
    """

    def __init__(self, path, prefix, code_length, timeout=None):
        self.prefix = prefix
        self.code_length = code_length
        self.timeout = timeout or None
        self._reader = InputReader(path)
        self._code = ""
        self._start = None
        
    def getCode(self):
        """
        Returns and then clears code if complete, else returns None
        """
        if self.timeout is not None and self._start is not None \
         and (time.time() - self._start) * 1000 >= self.timeout:
            self._code = ""
            self._time = None
            self._start = None
            return None
        self._reader.update()
        ascii_code_str = ""
        for i in range(len(self._reader["_type"])):
            if self._reader["_type"][i] == 1:
                ascii_code_str += Scanner.noneStr(self._reader["ascii_code"][i])
        if ascii_code_str:
            self._code += ascii_code_str
            if self._start is None:
                self._start = time.time()
            if not self._code.startswith(self.prefix):
                self._code = ""
                return None
        if len(self._code) >= self.code_length:
            self._code, c = "", self._code[:self.code_length] 
            self._time = None    
            return c
        return None

    @staticmethod
    def noneStr(s):
        return str(s) if s is not None else ""


class ScannerManagement(object):
    """
    Listens for scanners and keeps track of them 
    """
    
    def __init__(self, prefixes, base_path, code_length, clear_timeout=None, exec_code_func=(lambda:None), connect_scanner_func=(lambda:None), disconnect_scanner_func=(lambda:None)):
        """
        @param prefixes: dictionary of <prefix> : <name>
        @param base_path: base path to device files
        @param code_length: length of barcode excluding prefix and termination character
        @param exec_code_func: function to execute when code has been received with
                               signature: func(name, code, stripped_code, filename)
        """
        self.base_path = base_path
        self.code_length = code_length
        self._clear_timeout = clear_timeout
        self.exec_code_func = exec_code_func
        self.connect_scanner_func = connect_scanner_func
        self.disconnect_scanner_func = disconnect_scanner_func
        self._devices = {pre : {"name" : name, "obj" : None, "file" : None} for pre, name in prefixes.items()}
        self._all_devices = {}
        self._main_thread_run_event = Event()
        self._main_thread_run_event.set()
        self._main_thread = Thread(target=self._mainloop, args=(self._main_thread_run_event, ))
        self._main_thread.start()

    def _mainloop(self, run_event):
        while run_event.is_set():
            time.sleep(0.01)
            found_all_devices = True
            for pre, data in self._devices.items():
                if data["obj"] is None:
                    found_all_devices = False
                    continue;
                code = self.getCode(data["obj"])
                if code == -1:
                    self.disconnect_scanner_func(data["name"], data["file"])
                    self._devices[pre]["obj"] = None
                    self._devices[pre]["file"] = None
                    continue
                if code is not None:
                    # strip prefix and termination character 
                    stripped_code = code[1:self.code_length]
                    self.exec_code_func(data["name"], code, stripped_code, data["file"])

            if found_all_devices:
                continue
        
            self.catchAllDevices()
            for file, obj in list(self._all_devices.items()):
                code = self.getCode(obj)
                if code == -1:
                    del self._all_devices[file]
                    continue
                if code is not None and code[0] in self._devices and self._devices[code[0]]["obj"] is None:
                    # Add new scanner and execute code event handler
                    self._devices[code[0]]["obj"] = Scanner(os.path.join(self.base_path, file), code[0], self.code_length+1, self._clear_timeout)
                    self._devices[code[0]]["file"] = file
                    stripped_code = code[1:self.code_length]
                    self.exec_code_func(self._devices[code[0]]["name"], code, stripped_code, self._devices[code[0]]["file"])
                    self.connect_scanner_func(self._devices[code[0]]["name"], self._devices[code[0]]["file"])

    def getCode(self, obj):
        try:
            return obj.getCode()
        except OSError as e:
            # File does not exist => device is disconnected
            if e.errno == 19:
                return -1
            raise

    def catchAllDevices(self):
        """ Update list of all connected devices """
        files = os.listdir(self.base_path)
        needed_files = [x["file"] for x in self._devices.values() if x["obj"] is not None]
        # Add new devices to dict
        for file in files:
            if file not in self._all_devices and file not in needed_files:
                self._all_devices[file] = Scanner(os.path.join(self.base_path, file), "", self.code_length+1, self._clear_timeout)
        
    def stop(self):
        """ Stop main thread propperly """
        self._main_thread_run_event.clear()
        self._main_thread.join()

    def getDetectedScanners(self):
        return [x["file"] for x in self._devices.values() if x["obj"] is not None]

    def getNeededScannersCount(self):
        return len(self._devices)

    def getConnectedDevices(self):
        return list(self._all_devices.keys())

    def getStatus(self):
        return "Scanners found (%s/%s):\n\t- %s\nDevices connected (%s):\n\t- %s"%\
               ( len(self.getDetectedScanners()), self.getNeededScannersCount(), "\n\t- ".join(self.getDetectedScanners()),
                 len(self.getConnectedDevices()), "\n\t- ".join(self.getConnectedDevices()) )
            

class Server(object):
    HTTPError = urllib.error.HTTPError
    URLError = urllib.error.URLError
    
    def __init__(self, url, api_key):
        self.url = url;
        self.api_key = api_key

    def send(self, path, data):
        data = json.dumps(data).encode("utf-8")
        header = {"Content-Type" : "application/json"}
        req = urllib.request.Request(urllib.parse.urljoin(self.url, path), data, header)
        with urllib.request.urlopen(req) as resp:
            return resp.read().decode( resp.info().get_param("charset") or "utf-8" )
        
    def get(self, path, data=None):
        full_url = urllib.parse.urljoin(self.url, path)
        if data is not None:
            full_url += ('&' if urllib.parse.urlparse(full_url).query else '?') + urllib.parse.urlencode(data)
        header = {"Content-Type" : "application/json"}
        req = urllib.request.Request(full_url, None, header)
        with urllib.request.urlopen(req) as resp:
            return resp.read().decode( resp.info().get_param("charset") or "utf-8" )


class Main(object):
    def __init__(self):
        self.server = Server(SERVER_BASE_URL, SERVER_API_KEY)
        self.manager = ScannerManagement(CODE_PREFIXES, BASE_PATH, CODE_LENGTH, CLEAR_TIMEOUT, self.onReceiveCode, self.onConnectScanner, self.onDisconnectScanner)

        self._update_time_passed = 0
        self.database = {}
        self.readDatabase()
        self.updateDatabase()
        self.writeDatabase()
        
        try:
            self._mainloop()
        except KeyboardInterrupt as e:
            pass
        finally:
            self.manager.stop()
            
    def _mainloop(self):
        while True:
            if self._update_time_passed >= DATABASE_UPDATE_INTERVAL*60:
                self.updateDatabase()
                self.writeDatabase()
            self._update_time_passed += 1
            time.sleep(1)

    def readDatabase(self):
        try:
            with open(DATABASE_PATH) as f:
                self.database = json.load(f)
        except FileNotFoundError as e:
            with open(DATABASE_PATH, 'w') as f:
                pass
        except json.decoder.JSONDecodeError as e:
            pass

    def writeDatabase(self):
        with open(DATABASE_PATH, 'w') as f:
            json.dump(self.database, f)

    def updateDatabase(self):
        data = {"key": self.server.api_key}
        try:
            response = json.loads(self.server.get(SERVER_DATABASE_PATH, data))
            if self.checkResponse(response) is not None:
                self.database = response
                print("DATABASE: ", response)

        except Server.HTTPError as e:
            print("HTTP ERROR: %s" %e)
        except Server.URLError as e:
            print("URL ERROR: %s" %e)
        #except Exception as e:
        #    print("ERROR GETTING DATABASE: %s" %e)
    
    def onReceiveCode(self, name, full_code, stripped_code, filename):
        print("[%s]: %s | %s | %s" %(name, full_code, stripped_code, filename))
        data = {
            "key" : self.server.api_key,
            "gate" : name,
            "code" : stripped_code,
            "timestamp" : datetime.datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d %H:%M:%S"),
            "allow" : True
            
        }
        try:
            response = json.loads(self.server.send(SERVER_ADD_PATH, data))
            if self.checkResponse(response) is not None:
                print("RESPONSE", response)
        except Server.HTTPError as e:
            print("HTTP ERROR: %s" %e)
        except Server.URLError as e:
            print("URL ERROR: %s" %e)
        except Exception as e:
            print("ERROR SENDING DATA: %s" %e)

    def onConnectScanner(self, name, filename):
        print("[%s] connected! (%s)" %(name, filename))

    def onDisconnectScanner(self, name, filename):
        print("[%s] disconnected! (%s)" %(name, filename))

    def checkResponse(self, response):
        if response["status"] == "success":
            return response
        print("ERROR: ", response["msg"])
        return None
    
    def __del__(self):
        self.writeDatabase()


if __name__ == "__main__":
    Main()
