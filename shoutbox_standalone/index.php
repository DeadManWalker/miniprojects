<?php
   include_once("smileys.php");
   include_once("colors.php");
   include_once("symbols.php");
?>

<html>
   <head> 
      <link rel="stylesheet" type="text/css" href="index_style.css">
   </head>
   <div id="shoutbox_wrapper" class="boxShadow">
      <div id="shoutbox_output">
         <b> Loading... </b>
      </div> 
 
      <div id="shoutbox_input_name">
         <input type="text" placeholder="Hans Peter"> </input>
      </div> 
      <div>
         <div id="shoutbox_input_text">
            <input type="text" placeholder="Ich stinke" maxlength=200> </input>
         </div> 
         <div id="shoutbox_input_send">
            <button id="shoutbox_input_send_button" type="submit"> Send </button>
         </div>  
      </div>  

      <div id="shoutbox_options_wrapper">

         <div id="shoutbox_options_list">
            <div id="shoutbox_options_smiley_list">
               <?php
               foreach ($smileys as $key=>$val){
                  $img = $val[0];
                  echo "<button value=\"$key\"> <img src=\"$img\"/> </button>";
               }
               ?>
               </br> <span class="small"> Emoticons Copyright � Skype </span>
            </div>
            <div id="shoutbox_options_color_list">
               <?php
               foreach ($colors as $color){
                  echo "<button value=\"$color\" style=\"background: $color;\"> </button>";
               }
               ?>
            </div>
            <div id="shoutbox_options_symbol_list">
               <?php
               foreach ($symbols as $symbol){
                  echo "<button value=\"$symbol\"> $symbol </button>";
               }
               ?>
            </div>
         </div>  

         <div id="shoutbox_options_buttons">
            <div>
               <button class="bgSizeCover" id="shoutbox_button_smiley" type="button"> </button>
            </div>
            <div>
               <button id="shoutbox_button_color" type="button"> </button>
            </div>
            <div>
               <button id="shoutbox_button_symbol" type="button"> <?php echo $symbols[0] ?>  </button>
            </div>
         </div>
         <div class="copyright">
            Copyright � <?php echo date("Y"); ?> by Kevin Hambrecht. All rights reserved.
         </div>
      </div>   
   </div>
   <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
   <script type="text/javascript" src="carhartl-jquery-cookie-92b7715/jquery.cookie.js"></script>
   <script type="text/javascript" src="shoutbox.js"></script>
</html>