$(document).ready(function(){
   el_name = $("#shoutbox_input_name input");
   el_text = $("#shoutbox_input_text input");
   el_output = $("#shoutbox_output");
   el_button_color = $("#shoutbox_button_color");
   el_smiley_list = $("#shoutbox_options_smiley_list");
   el_color_list = $("#shoutbox_options_color_list");
   el_symbol_list = $("#shoutbox_options_symbol_list");

   getPhpArray();

   scroll = true;
   setInterval(function(){
      printData(scroll);
      scroll = false;
   }, 2000); 

  
   var last_username = $.cookie("shoutbox_username");
   if(last_username != "undefined"){
      el_name.val(last_username);
   }
   var last_color = $.cookie("shoutbox_color");
   if(last_color != "undefined"){
      el_button_color.css("background", last_color);
   }

   $("#shoutbox_wrapper").keypress(function(e){
      if (e.keyCode == 13){
         processData();
      }
   });
   
   $("#shoutbox_input_send").click(function(){
      processData();
   });

   $("#shoutbox_options_buttons button").mouseenter(function(){
      var id = this.id;
      if (id == "shoutbox_button_smiley"){
         el_smiley_list.css({"display": "block", "margin-top": -(el_smiley_list.height()+2)});
         el_color_list.css("display", "none");
         el_symbol_list.css("display", "none");
      }
      else if (id == "shoutbox_button_color"){
         el_color_list.css({"display": "block", "margin-top": -(el_color_list.height()+2)});
         el_smiley_list.css("display", "none");
         el_symbol_list.css("display", "none");
      }
      else if (id == "shoutbox_button_symbol"){
         el_symbol_list.css({"display": "block", "margin-top": -(el_symbol_list.height()+2)});
         el_smiley_list.css("display", "none");
         el_color_list.css("display", "none");
      }
   });

   $("#shoutbox_options_wrapper").mouseleave(function(){
      el_smiley_list.css("display", "none");
      el_color_list.css("display", "none");
      el_symbol_list.css("display", "none");
   });

   $("#shoutbox_options_smiley_list button").click(function(){
      el_smiley_list.css("display", "none");
      var smilie_name = $(this).val();
      var chat_smilie = smilies[smilie_name][1][0];
      var cur_position = el_text.getCursorPosition();
      var cur_content = el_text.val();
      var new_content = cur_content.substr(0, cur_position) + chat_smilie + 
                        cur_content.substr(cur_position);
      el_text.val(new_content);
      el_text.focus();
   });

   $("#shoutbox_options_symbol_list button").click(function(){
      el_symbol_list.css("display", "none");
      var symbol_name = $(this).val();
      var cur_position = el_text.getCursorPosition();
      var cur_content = el_text.val();
      var new_content = cur_content.substr(0, cur_position) + symbol_name + 
                        cur_content.substr(cur_position);
      el_text.val(new_content);
      el_text.focus();
   });

   $("#shoutbox_options_color_list button").click(function(){
      el_color_list.css("display", "none");
      var color = $(this).val();
      el_button_color.css("background", color);
      $.cookie("shoutbox_color", color, {expires: 7});
   });
})

function processData(){
   var name_value = el_name.val().trim().replace("<", "\<").replace(">", "\>");
   var text_value = el_text.val().trim().replace("<", "\<").replace(">", "\>");

   if (!name_value){
      el_name.css("borderColor", "rgb(255, 0, 0)");
      el_name.focus();
   }else{
      el_name.css("borderColor", "rgb(255, 255, 255)");
   }
   if (!text_value){
      el_text.css("borderColor", "rgb(255, 0, 0)");
      el_text.focus();
   }else{
      el_text.css("borderColor", "rgb(255, 255, 255)");
   }

   if (name_value && text_value){
      var date = new Date();
      var get_date = date.getDate();
      var get_month = date.getMonth() + 1;
      var get_year = date.getFullYear();
      var get_hour = date.getHours();
      var get_minute = date.getMinutes();
      var get_second = date.getSeconds();
      var new_date = get_date + '-' + get_month + '-' + get_year + ' ' + get_hour + ':' + get_minute + ':' + get_second;
      var color = el_button_color.css("backgroundColor");
      sendData(name_value, text_value, new_date, color);
      el_text.val("");
      $.cookie("shoutbox_username", name_value, {expires: 7});
      printData(true);
   }

}

function sendData(p_username, p_text, p_date, p_color){
   var ajax_call = $.ajax({
      type: "POST",
      url: "ajax_shoutbox_in.php",
      data: {username : p_username, text : p_text, date : p_date, color: p_color}
   })
   ajax_call.fail(function(xhr, ao, error){
      //alert("Error Data: " + xhr.status + ' - ' + error);
   })
}

function printData(scroll){
   var ajax_call = $.ajax({
      type: "POST",
      url: "ajax_shoutbox_out.php",
      dataType: "json"
   })
   ajax_call.fail(function(xhr, ao, error){
      //alert("Error Data: " + xhr.status + ' - ' + error);
   })
   ajax_call.success(function(data){
      el_output.empty();
      for (num in data){
         $.when(replaceWithSmilies(data[num].text, data[num].date, data[num].username, data[num].color)).then(function(data){
            el_output.append(
               "<div><span class='shoutbox_output_date'>" + data[1] + "</span></div>" +
               "<span class='shoutbox_output_username'> " + data[0] +
               "</span><span class='shoutbox_output_username'>:</span> <span class='shoutbox_output_text' style='color:" + data[3] + "'>" +
               data[2] + "</span></br>"
            );
            if (scroll){
               scrollDown(el_output);
            }
         })
      }
   })
}

function replaceWithSmilies(text, date, username, color){
   for (name in smilies){
      for (num in smilies[name][1]){
         text = text.replace(smilies[name][1][num], " <img src=\"" + smilies[name][0] + "\"/> ");
      }
   }
   return [username, date, text, color];
}


function getPhpArray(){
   var ajax_call = $.ajax({
      type: "POST",
      url: "ajax_get_array.php",
      dataType: "json"
   })
   ajax_call.success(function(data){
      smilies =  data;
   })
}

function scrollDown(id){
   id.scrollTop(id[0].scrollHeight)
}

(function($, undefined) {  
    $.fn.getCursorPosition = function() {  
        var el = $(this).get(0);  
        var pos = 0;  
        if ('selectionStart' in el) {  
            pos = el.selectionStart;  
        } else if ('selection' in document) {  
            el.focus();  
            var Sel = document.selection.createRange();  
            var SelLength = document.selection.createRange().text.length;  
            Sel.moveStart('character', -el.value.length);  
            pos = Sel.text.length - SelLength;  
        }  
        return pos;  
    }  
})(jQuery); 