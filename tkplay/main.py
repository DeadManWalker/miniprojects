#!/usr/bin/env python3


import tkinter
from tkinter import Tk, Frame, Canvas, TclError
from functools import partial
import time

from mixins import Debounce
from box import Box
from utils import Event, DynamicScalar


Box.Y_DIRRECTION = Box.Y_UP




class Collidable:
    _collidables = set()

    def __init__(self, *args, **kwargs):
        self._collidables.add(self)
        self.box.onChanged += self._positionChanged
        self.onCollided = Event()
        super().__init__()

    def _positionChanged(self, **kwargs):
        for collidable in self._collidables:
            (collidable is not self) and self._checkCollision(collidable, **kwargs)

    def _checkCollision(self, obj, **kwargs):
        s = self.box
        o = obj.box

        if s.lx < o.rx and s.rx > o.lx and s.ty > o.by and s.by < o.ty:
            self.onCollided(obj=obj, **kwargs)


class Entity:
    def __init__(self, *args, box=Box(), **kwargs):
        self.box = box
        self.children = []
        super().__init__()

    def update(self, delta_t):
        [child.update(delta_t) for child in self.children]



class Obstacle(Entity, Collidable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class Player(Collidable):
    def __init__(self):
        self.box = Box(size=(25, 50))

        self.dyn_x = DynamicScalar()
        self.dyn_y = DynamicScalar()
        self.speed = 500
        self.approach_s = 0.2
        self.jump_strength = 700
        self.gravity = 5000

        self.moving = 0
        self.jumping = 0
        self.in_air = False

        super().__init__()
        self.onCollided += self._collided


    def update(self, delta_t):
        self.dyn_y.acceleration -= self.gravity * delta_t
        if self.moving:
            self.dyn_x.approachVelocity(self.moving*self.speed, self.approach_s)

        if self.jumping and not self.in_air:
            self.dyn_y.velocity = self.jump_strength

        self.dyn_x.update(delta_t)
        self.dyn_y.update(delta_t)

        self.box.x += self.dyn_x.velocity * delta_t
        self.in_air = True
        self.box.y += self.dyn_y.velocity * delta_t



    def moveRight(self, *args):
        self.moving = 1

    def moveLeft(self, *args):
        self.moving = -1

    def jump(self, *args):
        self.jumping = 1

    def stopJump(self, *args):
        if self.jumping == 1:
            self.jumping = 0
            if self.in_air:
                self.dyn_y.velocity /= 3

    def stopMoveRight(self, *args):
        if self.moving == 1:
            self.dyn_x.approachVelocity(0, self.approach_s)
            self.moving = 0

    def stopMoveLeft(self, *args):
        if self.moving == -1:
            self.dyn_x.approachVelocity(0, self.approach_s)
            self.moving = 0


    def _collided(self, obj, property, new, old):
        if property == "x":
            self.dyn_x.acceleration = 0
            self.dyn_x.velocity = ((new < old) * 2 - 1) * self.speed/2
            self.dyn_x.approachVelocity(0, self.approach_s/2)
            self.box.x = old
        else:
            self.dyn_y.acceleration = 0
            self.dyn_y.velocity = 0
            self.box.y = old
            if old - new > 0:
                self.in_air = False


class Background(Entity):
    def __init__(self, size):
        super().__init__(box=Box(size=size))

    def update(self):
        pass


class Scene(Debounce, Canvas):
    def __init__(self, parent):
        super().__init__(parent)
        self.pack(fill=tkinter.BOTH, expand=1)

        self.parent = parent
        self.box = Box(size=(400, parent.GAME_SIZE[1]), cx=parent.GAME_SIZE[0]/2)
        self.box.y += parent.GAME_SIZE[1]

        self.background = Background(size=self.box.size)
        self.background.box.center = self.box.center

        self.grounds = [Obstacle(box=box) for box in Box.createBordersFromBox(self.box, border_width=10)]
        self.platforms = [
            Obstacle(box=Box(size=(self.box.width/3, 10), lx=self.box.lx, ty=self.box.cy/3)),
            Obstacle(box=Box(size=(self.box.width/3, 10), rx=self.box.rx, ty=self.box.cy*(2/3))),
            Obstacle(box=Box(size=(self.box.width/3, 10), lx=self.box.lx, ty=self.box.y*(2/3)))
        ]

        self.player = Player()
        self.player.box.bc = self.box.bc

        self.bind_all("<KeyPress-Left>", self.player.moveLeft)
        self.bind_all("<KeyPress-Right>", self.player.moveRight)
        self.bind_all("<KeyPress-space>", self.player.jump)

        self.bind_all("<KeyRelease-Left>", self.player.stopMoveLeft)
        self.bind_all("<KeyRelease-Right>", self.player.stopMoveRight)
        self.bind_all("<KeyRelease-space>", self.player.stopJump)

    def update(self, delta_t):
        super().update()
        self.updateGame(delta_t)
        self.draw()

    def updateGame(self, delta_t):
        self.player.update(delta_t)

    def draw(self):
        self.delete(tkinter.ALL)
        height = self.box.height
        things = [
            (self.background.box, "darkblue"),
            (self.player.box, "black"),
            *zip((g.box for g in self.grounds), ["white"]*len(self.grounds)),
            *zip((g.box for g in self.platforms), ["white"]*len(self.grounds))
        ]

        for box, color in things:
            tlbr = list(box.tlbr)
            tlbr[1] = -(tlbr[1] - height)
            tlbr[3] = -(tlbr[3] - height)
            self.create_rectangle(*tlbr, fill=color)




class Game(Frame):
    GAME_TITLE = "Thingy"
    GAME_SIZE = (800, 600)

    def __init__(self):
        self.root = Tk()
        self.root.title(self.GAME_TITLE)
        self.root.geometry(f"{self.GAME_SIZE[0]}x{self.GAME_SIZE[1]}+0+0")

        super().__init__(self.root)
        self.pack(fill=tkinter.BOTH, expand=1)

        self.timestamp = None
        self.running = False
        self.scene = Scene(self)

    def __del__(self):
        try:
            self.root.destroy()
        except TclError:
            pass


    def start(self):
        self.running = True
        self.timestamp = time.time()

        while self.running:
            now = time.time()
            self.scene.update(now - self.timestamp)
            self.timestamp = now
            self.root.update_idletasks()
            self.root.update()

    def stop(self):
        self.running = False





if __name__ == "__main__":

    game = Game()
    try:
        game.start()
    except KeyboardInterrupt:
        game.stop()