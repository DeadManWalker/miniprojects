#!/usr/bin/env python3



class Event:
    def __init__(self):
        self._callbacks = {}
        self._hold = False
        self._held_callbacks = set()
        self._kwargs = {}


    def fire(self, **kwargs):
        if self._hold:
            for cb, once in tuple(self._callbacks.items()):
                (self._held_callbacks.add(cb) or True) and once and self.unbind(cb)
            self._kwargs = kwargs
        else:
            for cb, once in tuple(self._callbacks.items()):
                (cb(**kwargs) or True) and once and self.unbind(cb)

    def __call__(self, **kwargs):
        self.fire(**kwargs)

    def bind(self, callback, once=False):
        self._callbacks[callback] = once

    def __iadd__(self, callback):
        self.bind(callback)
        return self

    def bindOnce(self, callback):
        self.bind(callback, once=True)

    def unbind(self, callback, strict=False):
        if strict:
            self._callbacks.pop(callback)
        else:
            self._callbacks.pop(callback, None)

    def hold(self):
        self._hold = True

    def __enter__(self):
        self.hold()

    def release(self):
        self._hold = False
        [cb(**self._kwargs) for cb in self._held_callbacks]
        self._held_callbacks = set()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()




class DynamicScalar(object):
    def __init__(self):
        self.acceleration = 0
        self.velocity = 0
        self.velocity_bounds = [None, None]
        self.acceleration_bounds = [None, None]

        self._target_velocity = None
        self._target_acceleration = None

    def approachVelocity(self, vel, seconds):
        self._target_velocity = [vel, seconds]
        self._target_acceleration = None

    def approachAcceleration(self, acc, seconds):
        self._target_acceleration = [acc, seconds]
        self._target_velocity = None

    def update(self, delta_t):
        if self._target_acceleration is not None:
            self._target_acceleration[1] -= delta_t
            if self._target_acceleration[1] < 0:
                self.acceleration = self._target_acceleration[0]
                self._target_acceleration = None
            else:
                self.acceleration = (self._target_acceleration[0] - self.acceleration) * self._target_acceleration[1]
        elif self._target_velocity is not None:
            self._target_velocity[1] -= delta_t
            if self._target_velocity[1] < 0:
                self.velocity = self._target_velocity[0]
                self.acceleration = 0
                self._target_velocity = None
            else:
                self.acceleration = (self._target_velocity[0] - self.velocity) / self._target_velocity[1]

        self.velocity += self.acceleration * delta_t

