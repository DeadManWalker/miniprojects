#!/usr/bin/env python3

from utils import Event



class Box:
    Y_DOWN = 1
    Y_UP = -1

    Y_DIRRECTION = Y_DOWN


    def __init__(self, **props):
        super().__init__()

        self.onChanged = Event()

        self._x = 0
        self._y = 0
        self._w = 0
        self._h = 0

        self.y_direction = self.Y_DIRRECTION

        for prop, value in props.items():
            self.__setattr__(prop, value)



    #
    # Left-X (X), Right-X, Top-Y (Y), Bottom-Y, Center-X, Center-Y
    #
    @property
    def lx(self):
        return self._x
    @lx.setter
    def lx(self, coord):
        self._x, old_x = coord, self._x
        if old_x != coord:
            self.onChanged(property="x", old=old_x, new=coord)

    x = lx

    @property
    def rx(self):
        return self.lx + self.width
    @rx.setter
    def rx(self, coord):
        self.lx = coord - self.width

    @property
    def ty(self):
        return self._y
    @ty.setter
    def ty(self, coord):
        self._y, old_y = coord, self._y
        if old_y != coord:
            self.onChanged(property="y", old=old_y, new=coord)

    y = ty

    @property
    def by(self):
        return self.ty + self.y_direction * self.height
    @by.setter
    def by(self, coord):
        self._y = coord - self.y_direction * self.height

    @property
    def cx(self):
        return self.lx + self.width/2
    @cx.setter
    def cx(self, coord):
        self.lx = coord - self.width/2

    @property
    def cy(self):
        return self.ty + self.y_direction * self.height/2
    @cy.setter
    def cy(self, coord):
        self.ty = coord - self.y_direction * self.height/2


    #
    # Top-Left, Top-Right, Bottom-Left, Bottom-Right
    #
    @property
    def tl(self):
        return (self.lx, self.ty)
    @tl.setter
    def tl(self, coords):
        with self.onChanged:
            self.lx = coords[0]
            self.ty = coords[1]

    @property
    def tr(self):
        return (self.rx, self.ty)
    @tl.setter
    def tr(self, coords):
        with self.onChanged:
            self.rx = coords[0]
            self.ty = coords[1]

    @property
    def bl(self):
        return (self.lx, self.by)
    @bl.setter
    def bl(self, coords):
        with self.onChanged:
            self.lx = coords[0]
            self.by = coords[1]

    @property
    def br(self):
        return (self.rx, self.by)
    @br.setter
    def br(self, coords):
        with self.onChanged:
            self.rx = coords[0]
            self.by = coords[1]

    #
    # Top-Center, Bottom-Center, Left-Center, Right-Center, Center
    #
    @property
    def tc(self):
        return (self.cx, self.ty)
    @tc.setter
    def tc(self, coords):
        with self.onChanged:
            self.cx = coords[0]
            self.ty = coords[1]

    @property
    def bc(self):
        return (self.cx, self.by)
    @bc.setter
    def bc(self, coords):
        with self.onChanged:
            self.cx = coords[0]
            self.by = coords[1]

    @property
    def lc(self):
        return (self.lx, self.cy)
    @lc.setter
    def lc(self, coords):
        with self.onChanged:
            self.lx = coords[0]
            self.cy = coords[1]

    @property
    def rc(self):
        return (self.rx, self.cy)
    @rc.setter
    def rc(self, coords):
        with self.onChanged:
            self.rx = coords[0]
            self.cy = coords[1]

    @property
    def center(self):
        return (self.cx, self.cy)
    @center.setter
    def center(self, coords):
        with self.onChanged:
            self.cx = coords[0]
            self.cy = coords[1]


    #
    # Width, Height, Size, Top-Left-Bottom-Right, Top-Left-Width-Height
    #
    @property
    def width(self):
        return self._w
    @width.setter
    def width(self, coord):
        self._w, old_w = coord, self._w
        if old_w != coord:
            self.onChanged(property="width", old=old_w, new=coord)

    @property
    def height(self):
        return self._h
    @height.setter
    def height(self, coord):
        self._h, old_h = coord, self._h
        if old_h != coord:
            self.onChanged(property="width", old=old_h, new=coord)

    @property
    def size(self):
        return (self.width, self.height)
    @size.setter
    def size(self, dim):
        with self.onChanged:
            self.width = dim[0]
            self.height = dim[1]

    @property
    def tlbr(self):
        return (*self.tl, *self.br)

    @property
    def tlwh(self):
        return (*self.tl, *self.size)



    #
    # Factory methods
    #
    @staticmethod
    def createBordersFromBox(box, border_width=1):
        return (
            Box(width=border_width, height=box.height + 2*border_width, rx=box.lx, cy=box.cy), #left
            Box(width=border_width, height=box.height + 2*border_width, lx=box.rx, cy=box.cy), #right
            Box(height=border_width, width=box.width, by=box.ty, cx=box.cx), #top
            Box(height=border_width, width=box.width, ty=box.by, cx=box.cx), #bottom
        )

