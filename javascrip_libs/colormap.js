

let Colormap = function(_options){
    let that = this;
    let options = {
        name: "jet",
        min: 0.0,
        max: 1.0
    };

    that.setOptions = function(_options){
        _options = _options || {};
        Object.keys(_options).forEach(function(key){
           options[key] = _options[key];
        });
    }
    that.setName = function(_name){
        options["name"] = _name;
    }
    that.setMin = function(_min){
        options["min"] = _min;
    }
    that.setMax = function(_max){
        options["max"] = _max;
    }
    that.getOptions = function(){
        return options;
    }

    let _get = function(_value){
        return Colormap.prototype.get(
            options["name"],
            _value,
            options["min"],
            options["max"]
        );
    }
    that.get = function(_value){;
        if(! Array.isArray(_value)){
            return _get(_value);
        }
        return _value.map(_get);
    }

    that.setOptions(_options);
}
Colormap.prototype = {
    get: function (_name, _value, _min, _max) {
        return Colormap.prototype.Maps[_name](
            Colormap.prototype.mapRange(_value, _min, _max)
        );
    },

    mapRange: function (_val, _min, _max) {
        return (_val * (_max - _min) + _min);
    },
    Maps: {
        "jet": function (value) {
            if (value < 1 / 8) {
                return new Colormap.prototype.Color(0, 0, (0.5 + value * 4));
            }
            else if (value < 3 / 8) {
                return new Colormap.prototype.Color(0, ((value - 1 / 8) * 4), 1);
            }
            else if (value < 5 / 8) {
                return new Colormap.prototype.Color(((value - 3 / 8) * 4), 1, 1 - ((value - 3 / 8) * 4));
            }
            else if (value < 7 / 8) {
                return new Colormap.prototype.Color(1, (1 - ((value - 5 / 8) * 4)), 0);
            }
            else if (value < 1) {
                return new Colormap.prototype.Color(((1 - value) * 4 + 0.5), 0, 0);
            }
            else {
                return new Colormap.prototype.Color(0.5, 0, 0);
            }
        },
        "jet2": function (value) {
            //http://vision.middlebury.edu/stereo/code/, Middlebury stereo evaluation code, file pfm2png.cpp
            value = value / 1.15 + 0.1;
            return new Colormap.prototype.Color(
                Math.max(0, Math.min(255, Math.round(255 * (1.5 - 4 * Math.abs(value - 0.75))))) / 255,
                Math.max(0, Math.min(255, Math.round(255 * (1.5 - 4 * Math.abs(value - 0.5))))) / 255,
                Math.max(0, Math.min(255, Math.round(255 * (1.5 - 4 * Math.abs(value - 0.25))))) / 255
            );
        },
        "autumn": function (value) {
            return new Colormap.prototype.Color(1, value, 0);
        },
        "gray": function (value) {
            return new Colormap.prototype.Color(value, value, value);
        },
        "bone": function (value) {
            return new Colormap.prototype.Color(
                ( (value < 36 / 48) ? value * 32 / 36 : (-1) / 3 + value * 4 / 3 ),
                ( (value < 18 / 48) ? value * 24 / 18 : (8 / 48) + value * 5 / 6 ),
                ( (value < 18 / 48) ? value * 16 / 18 : (value < 36 / 48) ? ((-1) / 6) + value * 4 / 3 : (16 / 48) + value * 2 / 3 )
            )
        },
        "cool": function (value) {
            return new Colormap.prototype.Color(value, (1 - value), 1);
        },
        "rainbow": function (value) {
            return new Colormap.prototype.Color(
                Math.round(Math.sin(0.024 * value * 255 + 0) * 127 + 128) / 255,
                Math.round(Math.sin(0.024 * value * 255 + 2) * 127 + 128) / 255,
                Math.round(Math.sin(0.024 * value * 255 + 4) * 127 + 128) / 255
            );
        },
        "long_rainbow": function (value) {
            let r, g, b;
            let a = (1 - value) / 0.2;
            let x = Math.floor(a);
            let y = Math.floor(255 * (a - x));
            switch (x) {
                case 0:
                    r = 255;
                    g = y;
                    b = 0;
                    break;
                case 1:
                    r = 255 - y;
                    g = 255;
                    b = 0;
                    break;
                case 2:
                    r = 0;
                    g = 255;
                    b = y;
                    break;
                case 3:
                    r = 0;
                    g = 255 - y;
                    b = 255;
                    break;
                case 4:
                    r = y;
                    g = 0;
                    b = 255;
                    break;
                case 5:
                    r = 255;
                    g = 0;
                    b = 255;
                    break;
            }
            return new Colormap.prototype.Color(r / 255.0, g / 255.0, b / 255.0);
        },
        "yellow_to_red": function (value) {
            let a = (1 - value);
            let y = Math.floor(255 * a);

            return new Colormap.prototype.Color(1, Math.floor(255 * (1 - value)) / 255.0, 0);
        },
        "hsv": function (value) {
            return new Colormap.prototype.Color.prototype.fromHSV(value, 1, 0.5);
        }
    }
}

Colormap.prototype.Color = function(r, g, b){
    let that = this;

    that.r = r || 0;
    that.g = g || 0;
    that.b = b || 0;

    that.rgb = function () {
        return [parseInt(that.r * 255), parseInt(that.g * 255), parseInt(that.b * 255)];
    }
    that.rgbString = function () {
        const rgb = that.rgb();
        return "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
    }
    let componentToHex = function (c) {
        const hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
    that.hex = function () {
        const rgb = that.rgb();
        return "#" + componentToHex(rgb[0]) + componentToHex(rgb[1]), componentToHex(rgb[2]);
    }
    that.hsv = function () {
        let _h, _s, _v, min_rgb, max_rgb, d, h;

        min_rgb = Math.min(that.r, Math.min(that.g, that.b));
        max_rgb = Math.max(that.r, Math.max(that.g, that.b));

        if (min_rgb === max_rgb) {
            return [0, 0, min_rgb];
        }
        d = (that.r === min_rgb) ? that.g - that.b :
            ((that.b == min_rgb) ? that.r - that.g : that.b - that.r);
        h = (that.r == min_rgb) ? 3 : ((that.b == min_rgb) ? 1 : 5);
        _h = 60 * (h - d / (max_rgb - min_rgb));
        _s = (max_rgb - min_rgb) / max_rgb;
        _v = max_rgb;

        return [_h, _s, _v];
    }
    that.cmyk = function () {
        if (!that.r && !that.g && !that.b) {
            return [0, 0, 0, 1];
        }

        let c, m, y, min_cmy;
        c = 1 - that.r;
        m = 1 - that.g;
        y = 1 - that.b;
        min_cmy = Math.min(c, Math.min(m, y));
        c = (c - min_cmy) / (1 - min_cmy);
        m = (m - min_cmy) / (1 - min_cmy);
        y = (y - min_cmy) / (1 - min_cmy);

        return [c, m, y, min_cmy];
    }
};
Colormap.prototype.Color.prototype = {
    fromHSV: function (h, s, v) {
        let r, g, b, p, q;

        let hueToRgb = function (p, q, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1 / 6) return (p + (q - p) * 6 * t);
            if (t < 1 / 2) return q;
            if (t < 2 / 3) return (p + (q - p) * (2 / 3 - t) * 6);
            return p;
        }

        if (s == 0) {
            r = g = b = v;
        } else {
            q = v < 0.5 ? v * (1 + s) : v + s - v * s;
            p = 2 * v - q;

            r = hueToRgb(p, q, h + 1 / 3.0);
            g = hueToRgb(p, q, h);
            b = hueToRgb(p, q, h - 1 / 3.0);
        }
        return new Colormap.prototype.Color(r, g, b);
    }
};