from classes import window, fancyimage


TEST_IMG = "/home/dmw/pictures/wallpapers/wallpaper.jpg"


def main():
   img = fancyimage.FancyImage(TEST_IMG)
   win = window.Window(img)

if __name__ == "__main__":
   main()
