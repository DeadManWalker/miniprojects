from PIL import Image, ImageTk, ImageDraw
from multiprocessing import Pool
import numpy as np
import random

import warnings

warnings.filterwarnings(action="ignore")


class ImgArray:
   def __init__(self, array, x, y):
      self.array = array
      self.x = x
      self.y = y

   def asTuple(self):
      return (self.array, self.x, self.y)

   def getSize(self):
      return (self.x, self.y)


class FancyImage(object):
   def __init__(self, img_path):
      self.path = img_path
      self.original_img = Image.open(self.path).convert("RGB")
      self.cur_img = self.original_img
      self.size = None
      #size, cur_img, cur_data, cur_np_data
      self.changeSize(self.original_img.size)
      self.resetImage()

      self.current_tk_image = None
      self.i=1

   def changeSize(self, size):
      if size is None or size == self.size:
         return
      self.size = size
      self.cur_img.thumbnail(self.size)
      temp_img = self.original_img.copy()
      temp_img.thumbnail(self.size)
      self.cur_data = temp_img.load()
      self.cur_np_data = np.array(temp_img, order="C")

   def resetImage(self):
      self.cur_img = Image.new(self.original_img.mode, self.size, (0,0,0))

   def update(self, size=None):
      print(self.i)
      self.changeSize(size)
      self.resetImage()
      num = (self.cur_img.size[0] * self.cur_img.size[1]) // 100

      padding = 5
      draw = ImageDraw.ImageDraw(self.cur_img)
      for _ in range(num):
         x, y = random.randint(0, self.cur_np_data.shape[0]-1), random.randint(0, self.cur_np_data.shape[1]-1)
         xy = (y-padding, x-padding, y+padding, x+padding)
         draw.ellipse(xy, tuple(self.cur_np_data[x][y]))

      self.i += 1

   def getImage(self):
      self.current_tk_image = ImageTk.PhotoImage(self.cur_img)
      return self.current_tk_image

