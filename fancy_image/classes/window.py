from tkinter import *
from time import time

class Window(object):
   def __init__(self, fancyimage_obj):
      self.fancyimage = fancyimage_obj
      self.window = Tk()
      self.canvas = ResizingCanvas(self.window)
      self.canvas.pack()

      self._bind()

      self._fullscreen_state = False;
      self.toggleFullscreen()
      self.mainloop()

   def mainloop(self):
      self.window.after(0, self.setImage)
      self.window.mainloop()

   def _bind(self):
      self.window.bind("<Configure>", self.on_configure)
      self.window.bind("<Return>", self.toggleFullscreen)
      self.window.bind("<Escape>", self.endFullscreen)
      self.window.bind("q", self.quit)

   def on_configure(self, event):
      self.canvas.resizeFit()

   def toggleFullscreen(self, event=None):
      self._fullscreen_state = not self._fullscreen_state
      self.window.attributes("-fullscreen", self._fullscreen_state)

   def endFullscreen(self, event=None):
      self._fullscreen_state = False
      self.window.attributes("-fullscreen", False)

   def quit(self, event=None):
      self.window.quit()

   def setImage(self):
      self.fancyimage.update(self.canvas.size)
      tk_image = self.fancyimage.getImage()
      self.canvas.create_image(0, 0, anchor=NW, image=tk_image)
      self.canvas.image = tk_image
      self.canvas.configure(width=tk_image.width(), height=tk_image.height())
      self.window.after(10, self.setImage)


class ResizingCanvas(Canvas):
   def __init__(self, parent, **kwargs):
      Canvas.__init__(self, parent, **kwargs)
      self.parent = parent
      #self.bind("<Configure>", self.on_resize)
      self.height = self.width = None
      self.resizeFit()

   @property
   def size(self):
      return (self.width, self.height)

   def resizeFit(self):
      self.resize(self.parent.winfo_width(), self.parent.winfo_height())

   def resize(self, w, h, scale=False):
      self.width = w
      self.height = h
      self.config(width=self.width, height=self.height)
      if scale:
         self.scale("all", 0, 0, float(w)/self.width, float(h)/self.height);

   def on_resize(self, event):
      #wscale = float(event.width)/self.width
      #hscale = float(event.height)/self.height
      self.resize(event.width, event.height)