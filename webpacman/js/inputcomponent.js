
let InputComponent = function(){
    let that = this;
    
    that.mouseButtonsDown = {};
    that.mouseButtonsUp = {};
    that.keysDown = {};
    that.keysUp = {};

    that.init = function(){
        document.addEventListener('keydown', that.keyDown, false);
        document.addEventListener('keyup', that.keyUp, false);
        document.addEventListener('mouseup', that.mouseUp, false);
        document.addEventListener('mousedown', that.mouseDown, false);
    }

    that.mouseDown = function(event){
        let eventobj = new InputComponent.Mouse(event);
        that.mouseButtonsDown[eventobj.buttonName.toLowerCase()] = eventobj;
    }
    that.mouseUp = function(event){
        let eventobj = new InputComponent.Mouse(event);
        that.mouseButtonsUp[eventobj.buttonName.toLowerCase()] = eventobj;
        delete that.mouseButtonsDown[eventobj.buttonName.toLowerCase()];
    }
    that.keyDown = function(event){
        let eventobj = new InputComponent.Key(event);
        that.keysDown[eventobj.char.toLowerCase()] = eventobj;
    }
    that.keyUp = function(event){
        let eventobj = new InputComponent.Key(event);
        that.keysUp[eventobj.char.toLowerCase()] = eventobj;
        delete that.keysDown[eventobj.char.toLowerCase()]
    }  

    that.getInput = function(){
        let mup = that.mouseButtonsUp;
        let kup = that.keysUp;
        that.mouseButtonsUp = {};
        that.keysUp = {};

        return {
            "mouseButtonsDown": that.mouseButtonsDown,
            "mouseButtonsUp": mup,
            "keysDown": that.keysDown,
            "keysUp": kup,
        };
    }
}
InputComponent.Key = function(event){
    this.char = event.char || event.key;
    this.repeat = event.repeat;
    this.pressedAt = Date.now();

    Object.defineProperty(this, "pressedFor", {
        "get" : ()=>{ return Date.now() - this.pressedAt }
    });
}
InputComponent.Mouse = function(event){
    this.button = event.button;
    this.buttonName = InputComponent.Mouse.Buttons[this.button];
    this.relatedTarget = event.relatedTarget;
    this.pressedAt = Date.now();
    this.clicks = event.detail;
    this.x = event.clientX;
    this.y = event.clientY;

    Object.defineProperty(this, "pressedFor", {
        "get" : ()=>{ return Date.now() - this.pressedAt }
    });
}
InputComponent.Mouse.Buttons = {
    0: "left",
    1: "middle",
    2: "right"
}

