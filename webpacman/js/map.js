

let MapTile = function(_is_wall, _directions){
    let that = this;
    let is_wall = _is_wall;
    that.directions = {
        "up": false,
        "right": false,
        "down": false,
        "left": false
    }
    Object.assign(that.directions, _directions);
    Object.defineProperty(that, 'is_wall', { get: function(){
        return is_wall;
    } });
}
MapTile.tilesize = 1;

let Map = function(){
    let that = this;
    that.tiles = [];
    that.players = [];
    that.enemies = [];
    let width, height;

    that.getWidth = function(){
        return width;
    }
    that.getHeight = function(){
        return height;
    }
    that.setSize = function(_width, _height){
        width = _width;
        height = _height;
        MapTile.tilesize = parseInt(Math.min( width/that.tiles[0].length, height/that.tiles.length ));
        // Set Character.size =  MapTile.tilesize?
    }

    that.getTileSize = function(){
        return MapTile.tilesize;
    }

    that.spawnPlayer = function(){
        let p = new Player();
        p.moveTo(new Point(MapTile.tilesize, MapTile.tilesize));
        that.players.push( p );
    }

    let processPlayerInput = function(inputobj){
        let player = that.players[0];
        let pos = player.getNodePosition();

        let checkMoveCond = function(direction){
            let tile = that.tiles[pos.y][pos.x];
            return (!tile.is_wall && tile.directions[direction]);
        }

        for(key of Object.keys(inputobj.keysDown)){
            switch(key){
                case "arrowup":
                    if(!checkMoveCond("up"))
                        break;
                    player.moveTo(new Point(pos.x*MapTile.tilesize, player.position.y));
                    player.moveUp();
                    break;
                case "arrowdown":
                    if(!checkMoveCond("down"))
                        break;
                    player.moveTo(new Point(pos.x*MapTile.tilesize, player.position.y));
                    player.moveDown();
                    break;
                case "arrowright":
                    if(!checkMoveCond("right"))
                        break;
                    player.moveTo(new Point(player.position.x, pos.y*MapTile.tilesize));
                    player.moveRight();
                    break;
                case "arrowleft":
                    if(!checkMoveCond("left"))
                        break;
                    player.moveTo(new Point(player.position.x, pos.y*MapTile.tilesize));
                    player.moveLeft();
                    break;
            }
        }
    }
    that.processInput = function(inputobj){
        processPlayerInput(inputobj);
        that.players.forEach((player) =>{
                player.processInput(inputobj);
        });
        that.enemies.forEach((enemy) =>{
            enemy.processInput(inputobj);
        });
    }
    that.update = function(step){
        that.players.forEach((player) =>{
            player.update(step);
        });
        that.enemies.forEach((enemy) =>{
            enemy.update(step);
        });
    }
    that.render = function(context){
        let tile;
        let size = MapTile.tilesize;
        for(let ty=0; ty<that.tiles.length; ++ty){
            for(let tx=0; tx<that.tiles[0].length; ++tx){
                tile = that.tiles[ty][tx];
                context.fillStyle = tile.is_wall ? "black" : "blue";
                context.beginPath();
                context.fillRect(size*tx, size*ty, size, size);
            }
        }

        that.players.forEach((player) =>{
            player.render(context);
        });
        that.enemies.forEach((enemy) =>{
            enemy.render(context);
        });
    }

}
Map.parse1dMask = function(arr, arr_width, arr_height){
    let arr2d = [];
    while(arr.length) arr2d.push(arr.splice(0, arr_width));
    
    return Map.parse2dMask(arr2d);
}
Map.parse2dMask = function(arr){
    let isValidPosition = function(x, y){
        return (x>=0 && x<arr[0].length && y>=0 && y<arr.length);
    }
    let getNeighbors = function(x, y){
        let dirs = {"up":false, "right":false, "down":false, "left":false};
        if(isValidPosition(x ,y-1))
            dirs["up"] = (arr[y-1][x] == true);
        if(isValidPosition(x+1 ,y))
            dirs["right"] = (arr[y][x+1] == true);
        if(isValidPosition(x ,y+1))
            dirs["down"] = (arr[y+1][x] == true);
        if(isValidPosition(x-1 ,y))
            dirs["left"] = (arr[y][x-1] == true);

        return dirs;
    }

    let map = new Map();
    let tile;

    map.tiles = [];
    for(let h=0; h<arr.length; ++h){
        map.tiles.push([]);
        for(let w=0; w<arr[0].length; ++w){
            tile = new MapTile( !arr[h][w], getNeighbors(w, h) );
            map.tiles[h].push( tile );
        }
    }
    
    return map;
}
