

let Point = function(x, y){
    let that = this;
    that.x = x || 0.0;
    that.y = y || 0.0;

    that.set = function(x, y){
        that.x = x;
        that.y = y;
        return that;
    }
    that.add = function(x, y){
        that.x += x;
        that.y += y;
        return that;
    }
    that.mult = function(m){
        that.x *= m;
        that.y *= m;
        return that;
    }
    that.copy = function(){
        return new Point(that.x, that.y);
    }
    that.map = function(func){
        return new Point( func(that.x), func(that.y) );
    }
}
Point.fromPoint = function(p){
    return new Point(p.x, p.y);
}

let Rect = function(width, height){
    let that = this;

    that.topleft = new Point();
    that.topright = new Point(width, 0.0);
    that.bottomright = new Point(width, height);
    that.bottomleft = new Point(0.0, height);

    Object.defineProperty(that, 'width', { get: function(){
        return that.topright.x-that.topleft.x;
    } });
    Object.defineProperty(that, 'height', { get: function(){
        return that.bottomleft.y-that.topleft.y;
    } });

    that.setTopleft = function(p){
        let diffx = p.x - that.topleft.x;
        let diffy = p.y - that.topleft.y;

        that.shift(new Point(diffx, diffy));

        return that;
    }
    that.setCenter = function(p){
        let diffx = that.topleftx + that.width/2.0 - p.x;
        let diffy = p.y - that.toplefty + that.height/2.0;

        that.shift(new Point(diffx, diffy));
        return that;
    }
    that.shift = function(p){
        that.topleft.add(p.x, p.y);
        that.topright.add(p.x, p.y);
        that.bottomright.add(p.x, p.y);
        that.bottomleft.add(p.x, p.y);
        return that;
    }
    that.copy = function(){
        return new Rect(that.topleft, that.width, that.height);
    }

    that.getCenter = function(){
        return new Point(that.topleft.x + that.width/2.0, that.topleft.y + that.height/2.0);
    }

}
Rect.fromPoints = function(p1, p2, p3, p4){
    let rect = new Rect();
    rect.topleft = p1;
    rect.topright = p2;
    rect.bottomright = p3;
    rect.bottomleft = p4;

    return rect;
}
Rect.fromTopleft = function(p, width, height){
    let rect = new Rect();
    rect.topleft = p;
    rect.topright.set(p.x+width, p.y);
    rect.bottomright.set(p.x+width, p.y+height);
    rect.bottomleft.set(p.x, p.y+height);

    return rect;
}