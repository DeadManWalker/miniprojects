
let Sprite = function(){
    let that = this;
    that.canvas = document.createElement("canvas");
    that.context = that.canvas.getContext("2d");
    that.rect = new Rect();

    that.render = function(context){
        context.drawImage(that.canvas, that.rect.topleft.x, that.rect.topleft.y);
    }
}
Sprite.fromImg = function(img){
    let sprite = new Sprite();
    sprite.canvas.width = img.width;
    sprite.canvas.height = img.height;
    sprite.context.drawImage(img);
    sprite.rect = new Rect(img.width, img.height);

    return sprite;
}
Sprite.fromColor = function(color, rect){
    let sprite = new Sprite();
    sprite.rect = rect;
    sprite.canvas.width = rect.width;
    sprite.canvas.height = rect.height;
    sprite.context.fillStyle = color;
    sprite.context.beginPath();
    sprite.context.fillRect(rect.topleft.x, rect.topleft.y, rect.width, rect.height);

    return sprite;
}