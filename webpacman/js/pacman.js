
let Pacman = function(_canvas){
    let that = this;
    let last_timestamp = 0;
    let canvas;
    let context;
    let map;
    let anim_frame_id;
    let cInput = new InputComponent();

    that.setCanvas = function(_canvas){
        canvas = _canvas;  
        context = canvas.getContext("2d");
    }
    that.resize = function(){        
        map.setSize(canvas.width, canvas.height);
    }

    that.setMap = function(_map){
        map = _map;
    }
    that.spawnPlayer = function(){
        map.spawnPlayer();
    }
    that.spawnEnemy = function(tx, ty){

    }

    that.start = function(){

    }
    that.pause = function(){

    }
    that.end = function(){

    }

    that.processInput = function(){
        map.processInput( cInput.getInput() );
    }
    that.update = function(step){
        map.update(step);
    }
    that.render = function(){
        context.clearRect(0, 0, canvas.width, canvas.height);
        map.render(context);
    }

    that.mainLoop = function(timestamp){
        let step = timestamp - last_timestamp;
        last_timestamp = timestamp;
     
        that.processInput();
        that.update(step);
        that.render();

        anim_frame_id = window.requestAnimationFrame(that.mainLoop);    
    }  

    that.setCanvas(_canvas);

    that.init = function(){
        cInput.init();
        that.setMap(Map.parse2dMask([
            [0,0,1,0,0,0,0,0,0,1,0],
            [1,1,1,1,1,1,1,1,1,1,1], 
            [0,1,0,1,0,0,0,0,0,1,0],
            [0,1,0,1,1,1,0,0,0,1,0],
            [1,1,0,0,0,1,1,1,1,1,1], 
            [1,0,0,0,0,1,0,0,0,1,0],
            [1,1,1,1,1,1,1,1,1,1,1],
            [0,0,1,0,0,0,0,0,0,1,0]
        ]));
        map.setSize(canvas.width, canvas.height);
        that.spawnPlayer();

        window.cancelAnimationFrame(anim_frame_id);
        anim_frame_id = window.requestAnimationFrame(that.mainLoop);
    }
}

