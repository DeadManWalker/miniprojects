
let Character = function(){
    let that = this;
    let step = new Point();
    that.speed = Character.NORMAL_SPEED;
    that.sprite = Sprite.fromColor("red", new Rect(50.0, 50.0));
    that.position = that.sprite.rect.topleft;

    that.processInput = function(inputobj){
    }
    that.update = function(_step){
        that.sprite.rect.shift(step.copy().mult(_step));
    }
    that.render = function(context){
        that.sprite.render(context);
    }

    that.moveTo = function(p){
        console.log("POINT: ", p);
        that.sprite.rect.setTopleft(p);//setCenter(p);
    }
    that.moveUp = function(){
        step.set(0.0, 0.0);
        step.y -= that.speed;
    }
    that.moveDown = function(){
        step.set(0.0, 0.0);
        step.y += that.speed;
    }
    that.moveRight = function(){
        step.set(0.0, 0.0);
        step.x += that.speed;
    }
    that.moveLeft = function(){
        step.set(0.0, 0.0);
        step.x -= that.speed;
    }
    that.stop = function(){
        step.set(0.0, 0.0);
    }

    that.getNodePosition = function(){
        return that.sprite.rect.getCenter().map(
            (x) => {return (parseInt(x/MapTile.tilesize))}
        );
    }
}
Character.NORMAL_SPEED = 0.01;
