    
let Player = function(){
    Character.call(this);
    let that = this;
    that.speed = Player.NORMAL_SPEED;
    that.last_dir = null;
}
Player.NORMAL_SPEED = 0.1;

Character.prototype = Object.create(Player.prototype);
Player.prototype.constructor = Player;