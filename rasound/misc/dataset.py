#!/usr/bin/env python3

import urllib.request
import urllib.parse
import os.path
import json
from pprint import pprint

from bs4 import BeautifulSoup


YT_API_KEY = "AIzaSyCt7PkKtIlJl_uVw-lRpymNR49lD919gFQ"
YT_PLAYLIST_ID = "PLVzlUNhyeioT1XMe-U12rfcozOiN6dtyE"

VIDEOS_FILEPATH = "./videos.json"
ARTISTS_FILEPATH = "./artists.txt"

_videos = None
_artists = None


class YtPlaylist(object):
    YT_API_URL_BASE = "https://www.googleapis.com/youtube/v3"

    def __init__(self, api_key, playlist_id):
        self.api_key = api_key
        self.playlist_id = playlist_id
        self.max_results = 50
        self.videos = {}
        self.video_item_filter = self.titleDescriptionFilter
        self.print_progress = True

    def _buildUrl(self, page_token=None):
        params = {
            "key": self.api_key,
            "playlistId": self.playlist_id,
            "part": "snippet",
            "maxResults": self.max_results
        }
        if page_token is not None:
            params["pageToken"] = page_token
        params = urllib.parse.urlencode(params)
        return os.path.join(self.YT_API_URL_BASE, "playlistItems") + "?" + params

    @staticmethod
    def titleDescriptionFilter(video_item):
        snippet = video_item["snippet"]
        return {
            snippet["resourceId"]["videoId"]: {
                "title": snippet["title"],
                "description": snippet["description"]
            }        
        }

    def queryVideos(self):
        total_results = 1
        result_count = 0
        next_page_token = None
        if self.print_progress:
            print("Querying Videos...")

        while result_count < total_results:
            url = self._buildUrl(page_token=next_page_token)
            with urllib.request.urlopen(url) as f:
                resp = json.load(f)
            total_results = int(resp["pageInfo"]["totalResults"])
            result_count += int(resp["pageInfo"]["resultsPerPage"])
            if self.print_progress:
                print(f"\t{result_count}/{total_results}")
            for item in resp["items"]:
                self.videos.update(self.video_item_filter(item))
            try:
                next_page_token = resp["nextPageToken"]
            except KeyError:
                break
        return self.videos


def getVideos():
    global _videos

    if _videos is not None:
        return _videos

    try:
        with open(VIDEOS_FILEPATH) as f:
            _videos = json.load(f)
            return _videos
    except IOError:
        _videos = YtPlaylist(YT_API_KEY, YT_PLAYLIST_ID).queryVideos()
        with open(VIDEOS_FILEPATH, "w") as f:
            json.dump(_videos, f)
        return _videos


def titles():
    for video in getVideos().values():
        yield video["title"]

def descriptions():
    for video in getVideos().values():
        yield video["description"]

def ids():
    for video_id in getVideos():
        yield video_id

def videos():
    for video in getVideos():
        yield video

def artists():
    global _artists
    
    if _artists is not None:
        return _artists

    try:
        with open(ARTISTS_FILEPATH) as f:
            _artists = f.read().split("\n")
        return _artists
    except IOError:
        URL = "https://en.wikipedia.org/wiki/List_of_reggae_musicians"
        with urllib.request.urlopen(URL) as f:
            soup = BeautifulSoup(f.read(), "html.parser")
        links = soup.select("h2 ~ div > ul > li > a[title]")       
        artists = [link.string for link in links]
        with open(ARTISTS_FILEPATH, "w") as f:
            f.writelines("\n".join(artists))
        _artists = artists
        return _artists
 
if __name__ == "__main__":
    videos = getVideos()
    pprint(list(titles(videos)))
