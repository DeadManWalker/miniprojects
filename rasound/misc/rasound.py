#!/usr/bin/env python3

from pprint import pprint
import re

import gensim.models
import dataset

MODEL_FILEPATH = "w2v.model"


STOPLIST = set('for a of the and to in'.split(' '))  
def preprocessSentence(sentence):
    s = sentence.lower()
    #s = re.sub(r"[^0-9a-zA-Z ]", "", s)
    s = re.sub(r"\s+", " ", s)
    #s = [s for s in s.split() if s not in STOPLIST]
    s = s.split()    
    return s

def tokenize(data):
    sentences = []
    for sentence in data:
        sentences.append(preprocessSentence(sentence))
    return sentences
        

def getW2VModel(data):
    try:
        model = gensim.models.Word2Vec.load(MODEL_FILEPATH)
    except FileNotFoundError:
        print("Training Model...")
        sentences = tokenize(data)
        model = gensim.models.Word2Vec(sentences=sentences, min_count=3, size=200, workers=4, window=6, iter=100, sg=0)
        model.save(MODEL_FILEPATH)
        print("Done!")
    return model


def prepareTitles(titles):
    def cutMatches(pattern, string):
        matches = re.findall(pattern, string)
        rest = re.sub("|".join([re.escape(m) for m in matches]), "", string)
        return (rest, matches)

    def extractTags(string, tag, *tags):
        tags += (tag, )
        s = string
        groups = []
        for topen, tclose in tags:
            s, g = cutMatches(r"(%s.*?%s)" %(re.escape(topen), re.escape(tclose)), s)
            groups += [i[1:-1] for i in g]
        return s, groups

    for title in titles:
        t = title.lower()
        t = re.sub(r"'", "", t)
        t = re.sub(r"&", " and ", t)
        t = re.sub(r"[^a-zA-Z0-9\-\(\)\[\]\|]+", " ", t)
        t = re.sub(r"\s+", " ", t)
        t, groups = extractTags(t, ("(", ")"), ("[", "]"), ("{", "}"), ('"', '"'))
        parts = re.split(r"[\-\|]", t)
        yield [el.strip().split() for el in parts + groups if el.strip()]
        
        

def main():
    titles = list(dataset.titles())
    preped = list(prepareTitles(titles))

    artists = list(prepareTitles(dataset.artists()))
    pprint(artists)


    """
    import numpy as np
    flattened = [el for title in preped for el in title]   
    u, c = np.unique(flattened, return_counts=True)
    uc = sorted(list(zip(u, c)), key=lambda x :x[1])
    pprint(uc)
    return
    for t, p in zip(titles, preped):
        print(t)
        print(p)
        print()
    """

    """
    model = getW2VModel(titles)
    context = ["fantan"]
    pprint(model.predict_output_word(context, topn=10))
    pprint(model.wv.most_similar(context, topn=10))    
    pprint(len(model.wv.vocab))
    """


if __name__ == "__main__":
    main()
