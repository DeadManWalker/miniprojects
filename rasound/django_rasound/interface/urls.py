from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name="rasinterface_index"),
    path('ytplaylist/<id>/', views.listYoutubePlaylist, name="rasinterface_ytplaylist"),
]
