import urllib
import os.path
import json
import re

from bs4 import BeautifulSoup
from fuzzywuzzy import fuzz


from api.models import Artist

class PlaylistLoader(object):
    YT_API_URL_BASE = "https://www.googleapis.com/youtube/v3"

    def __init__(self, api_key, playlist_id):
        self.api_key = api_key
        self.playlist_id = playlist_id
        self.max_results = 50

    def _buildUrl(self, page_token=None):
        params = {
            "key": self.api_key,
            "playlistId": self.playlist_id,
            "part": "snippet",
            "maxResults": self.max_results
        }
        if page_token is not None:
            params["pageToken"] = page_token
        params = urllib.parse.urlencode(params)
        return os.path.join(self.YT_API_URL_BASE, "playlistItems") + "?" + params


    def __iter__(self):
        total_results = 1
        result_count = 0
        next_page_token = None

        while result_count < total_results:
            url = self._buildUrl(page_token=next_page_token)
            with urllib.request.urlopen(url) as f:
                resp = json.load(f)
            total_results = int(resp["pageInfo"]["totalResults"])
            result_count += int(resp["pageInfo"]["resultsPerPage"])
            for item in resp["items"]:
                yield item["snippet"]
            try:
                next_page_token = resp["nextPageToken"]
            except KeyError:
                break


class ReggaeArtists(object):
    URL = "https://en.wikipedia.org/wiki/List_of_reggae_musicians"

    def __init__(self):
        self._artists = set()
        self.loadDbArtists()
        self.loadWikiArtists()

    @property
    def artists(self):
        return self._artists

    def loadDbArtists(self):
        self._artists |= set(self.normalize(name) for name in Artist.objects.all().values_list("name", flat=True))

    def loadWikiArtists(self):
        with urllib.request.urlopen(self.URL) as f:
            soup = BeautifulSoup(f.read(), "html.parser")
        links = soup.select("h2 ~ div > ul > li > a[title]")
        self._artists |= set(self.normalize(link.string) for link in links)

    def normalize(self, name):
        return name.lower()

    def __iter__(self):
        return iter(self.artists)



class TitleProcessor(object):
    @classmethod
    def cutMatches(cls, pattern, string):
        matches = re.findall(pattern, string)
        rest = re.sub("|".join([re.escape(m) for m in matches]), "", string)
        return (rest, matches)

    @classmethod
    def extractTags(cls, string, tag, *tags):
        tags += (tag,)
        s = string
        groups = []
        for topen, tclose in tags:
            s, g = cls.cutMatches(r"(%s.*?%s)" % (re.escape(topen), re.escape(tclose)), s)
            groups += [i[1:-1] for i in g]
        return s, groups

    @classmethod
    def process(cls, title):
        t = title.lower()
        t = re.sub(r"'", "", t)
        t = re.sub(r"&", " and ", t)
        t = re.sub(r"[^a-zA-Z0-9\-\(\)\[\]\|]+", " ", t)
        t = re.sub(r"\s+", " ", t)
        t, groups = cls.extractTags(t, ("(", ")"), ("[", "]"), ("{", "}"), ('"', '"'))
        parts = re.split(r"[\-\|]", t)
        return [el.strip().split() for el in parts + groups if el.strip()]



class Predict(object):
    @classmethod
    def artistsFromString(cls, artists, string):
        pred_artists = set()
        for artist in artists:
            if fuzz.token_set_ratio(string, artist.lower()) > 90:
                pred_artists.add(artist)
        return pred_artists


    @classmethod
    def artistsFromRawTitle(cls, artists, raw_title):
        groups = TitleProcessor.process(raw_title)
        pred_artists = set()
        title_group = []

        for words in groups:
            string = " ".join(words)
            p_artists = cls.artistsFromString(artists, string)
            if p_artists:
               pred_artists |= p_artists
            else:
                title_group.append(string)

        return (pred_artists, " ".join(title_group).title())






def loadPlaylist(id, api_key):
    artists = ReggaeArtists()
    for video in PlaylistLoader(api_key=api_key, playlist_id=id):
        title = video["title"]
        pred_artists, pred_title = Predict.artistsFromRawTitle(artists, title)

        yield {
            "title": title,
            "id": video["resourceId"]["videoId"],
            "predicted_artists": pred_artists,
            "predicted_title" : pred_title
        }


