from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.db.utils import IntegrityError

from api.models import User, Song, Source, Artist, Genre
from interface import youtubeLoader



def index(request, *args, **kwargs):
    songs_data = []
    songs = Song.objects.prefetch_related("genres", "artists", "sources")
    for song in songs:
        songs_data.append({
            "id" : song.id,
            "name": song.name,
            "created": song.created,
            "genres": list( song.genres.values("id", "name") ),
            "artists": list( song.artists.values("id", "name") ),
            "sources": list( song.sources.values("id", "uri") ),
        })

    context = {"songs": songs_data}
    return render(request, "index.html", context)



def saveYoutubeSong(yt_song_data):
    try:
        song = Song.objects.create(name=yt_song_data["predicted_title"])
    except IntegrityError:
        return
    for artist in yt_song_data["predicted_artists"]:
        print(artist)
        a = Artist.objects.get_or_create(name__iexact=artist, defaults={"name": artist})[0]
        song.artists.add(a)
    try:
        Source.objects.create(uri=f"https://www.youtube.com/watch?v={yt_song_data['id']}", type="youtube", song=song)
    except IntegrityError:
        pass
    g = Genre.objects.get_or_create(name__iexact="reggae", defaults={"name": "Reggae"})[0]
    song.genres.add(g)


def listYoutubePlaylist(request, *args, **kwargs):
    try:
        yt_api_key = request.GET["yt_api_key"]
    except KeyError:
        return JsonResponse({"Error": "'yt_api_key' parameter must be specified"})

    pl_id = kwargs["id"]
    try:
        max_count = int(request.GET["count"])
    except (KeyError, ValueError):
        max_count = 50

    videos = []
    for i, video in enumerate(youtubeLoader.loadPlaylist(id=pl_id, api_key=yt_api_key), start=1):
        videos.append(video)
        saveYoutubeSong(video)

        if i >= max_count:
            break

    context = {"videos": videos}
    return render(request, "ytplaylist.html", context)
    return JsonResponse(data, safe=False)
