from django.db import models



class User(models.Model):
    name = models.CharField(max_length=20)
    created = models.DateTimeField(auto_now_add=True)
    salt = models.BinaryField(max_length=64)
    password = models.BinaryField(max_length=64)


class Song(models.Model):
    name = models.CharField(max_length=30)
    created = models.DateTimeField(auto_now_add=True)
    artists = models.ManyToManyField("Artist", through="SongArtistMap")
    genres = models.ManyToManyField("Genre", through="SongGenreMap")
    users = models.ManyToManyField("User", through="SongUserMap")


class Artist(models.Model):
    name = models.CharField(max_length=20, unique=True)
    created = models.DateTimeField(auto_now_add=True)


class Genre(models.Model):
    name = models.CharField(max_length=20, unique=True)
    created = models.DateTimeField(auto_now_add=True)


class Source(models.Model):
    class SourceType(models.TextChoices):
        LOCAL = ("LC", "Local")
        WEB_SERVICE_URL = ("WS", "Web Service Url")

    uri = models.CharField(max_length=40, unique=True)
    song = models.ForeignKey(Song, on_delete=models.CASCADE, related_name="sources")
    type = models.CharField(max_length=2, choices=SourceType.choices, default=None)
    created = models.DateTimeField(auto_now_add=True)


class SongArtistMap(models.Model):
    class ArtistType(models.TextChoices):
        FEATURED = ("FT", "Featured")
        MAIN = ("MN", "Main")

    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    type = models.CharField(max_length=2, choices=ArtistType.choices, default=ArtistType.MAIN)
    created = models.DateTimeField(auto_now_add=True)


class SongGenreMap(models.Model):
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)


class SongUserMap(models.Model):
    class SongUserRating(models.IntegerChoices):
        UNRATED = (-1, "Unrated")
        ONE = (1, "1")
        TWO = (2, "2")
        THREE = (3, "3")
        FOUR = (4, "4")
        FIVE = (5, "5")

    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.IntegerField(choices=SongUserRating.choices, default=SongUserRating.UNRATED)
    created = models.DateTimeField(auto_now_add=True)