from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.db.utils import IntegrityError


from .models import User, Song, Source, Artist, Genre


def index(request):
    data = {

    }
    return JsonResponse(data)



@method_decorator(csrf_exempt, name='dispatch')
class UserView(View):
    def get(self, request, *args, **kwargs):
        result = list( User.objects.all().order_by("created").values_list("id", flat=True) )
        return JsonResponse(result, safe=False)

    def post(self, request, *args, **kwargs):
        try:
            name = request.POST["name"]
            pw = request.POST["password"]
        except KeyError:
            return JsonResponse({"ERROR": "must specify 'name' and 'password'"}, safe=False)
        user = User.objects.create(name=name, password=pw)
        return JsonResponse({"User Created": user.id}, safe=False)


class UserDetailView(View):
    def get(self, request, *args, **kwargs):
        id = kwargs["id"]
        try:
            result = User.objects.get(id=id).values("id", "created")
        except Genre.DoesNotExist:
            return JsonResponse({"User does not exist": id}, safe=False)
        return JsonResponse(result, safe=False)




@method_decorator(csrf_exempt, name='dispatch')
class SongView(View):
    def get(self, request, *args, **kwargs):
        result = list( Song.objects.all().order_by("created").values_list("id", flat=True) )
        return JsonResponse(result, safe=False)

    def post(self, request, *args, **kwargs):
        try:
            name = request.POST["name"]
        except KeyError:
            return JsonResponse({"ERROR": "must specify 'name'"}, safe=False)
        try:
            song = Song.objects.create(name=name)
        except IntegrityError:
            return JsonResponse({"ERROR": f"Name '{name}' already exists"}, safe=False)

        genre_ids = request.POST.getlist("genres")
        artist_ids = request.POST.getlist("artists")
        source_ids = request.POST.getlist("sources")
        genres = Genre.objects.all().filter(id__in=genre_ids)
        artists = Artist.objects.all().filter(id__in=artist_ids)
        sources = Source.objects.all().filter(id__in=source_ids)

        song.genres.add(*genres)
        song.artists.add(*artists)
        song.sources.add(*sources)

        return JsonResponse({"Song Created": song.id}, safe=False)

class SongDetailView(View):
    def get(self, request, *args, **kwargs):
        id = kwargs["id"]
        try:
            #result = dict( Song.objects.values().get(id=id) )
            r = Song.objects.filter(id=id)
        except Song.DoesNotExist:
            return JsonResponse({"Song does not exist": id}, safe=False)

        a = list(r.values_list("artists__id", flat=True))
        g = list(r.values_list("genres__id", flat=True))
        s = list(r.values_list("sources__id", flat=True))
        result = dict(*r.values(), artists=a, genres=g, sources=s)
        return JsonResponse(result, safe=False)



@method_decorator(csrf_exempt, name='dispatch')
class SourceView(View):
    def get(self, request, *args, **kwargs):
        result = list( Source.objects.all().order_by("created").values_list("id", flat=True) )
        return JsonResponse(result, safe=False)

    def post(self, request, *args, **kwargs):
        try:
            uri = request.POST["uri"]
            song_id = request.POST["song"]
        except KeyError:
            return JsonResponse({"ERROR": "must specify 'uri' and 'song'"}, safe=False)
        stype = request.POST.get("type", None)
        try:
            source = Source.objects.create(uri=uri, song=song_id, type=stype)
        except IntegrityError:
            return JsonResponse({"ERROR": f"Name '{uri}' already exists"}, safe=False)

        return JsonResponse({"Source Created": source.id}, safe=False)

class SourceDetailView(View):
    def get(self, request, *args, **kwargs):
        id = kwargs["id"]
        try:
            result = Source.objects.values("id", "uri", "type", "song").get(id=id)
        except Genre.DoesNotExist:
            return JsonResponse({"Source does not exist": id}, safe=False)

        return JsonResponse(result, safe=False)



@method_decorator(csrf_exempt, name='dispatch')
class GenreView(View):
    def get(self, request, *args, **kwargs):
        result = list( Genre.objects.all().order_by("created").values_list("id", flat=True) )
        return JsonResponse(result, safe=False)

    def post(self, request, *args, **kwargs):
        try:
            name = request.POST["name"].strip()
            if not len(name):
                raise ValueError
        except (KeyError, ValueError):
            return JsonResponse({"ERROR": "must specify 'name'"}, safe=False)

        try:
            genre = Genre.objects.create(name=name)
        except IntegrityError:
            return JsonResponse({"ERROR": f"Name '{name}' already exists"}, safe=False)

        song_id = request.POST.get("song", None)
        if song_id:
            try:
                song = Song.objects.get(id=song_id)
            except Song.DoesNotExist:
                return JsonResponse({"ERROR": f"Song id '{song_id}' does not exist"}, safe=False)
            song.artists.add(genre)

        return JsonResponse({"Genre Created": genre.id}, safe=False)


class GenreDetailView(View):
    def get(self, request, *args, **kwargs):
        id = kwargs["id"]
        try:
            result = Genre.objects.values("id", "name").get(id=id)
        except Genre.DoesNotExist:
            return JsonResponse({"Genre does not exist": id}, safe=False)

        return JsonResponse(result, safe=False)



@method_decorator(csrf_exempt, name='dispatch')
class ArtistView(View):
    def get(self, request, *args, **kwargs):
        result = list( Artist.objects.all().order_by("created").values_list("id", flat=True) )
        return JsonResponse(result, safe=False)

    def post(self, request, *args, **kwargs):
        try:
            name = request.POST["name"].strip()
            if not len(name):
                raise ValueError
        except (KeyError, ValueError):
            return JsonResponse({"ERROR": "must specify 'name'"}, safe=False)

        try:
            artist = Artist.objects.create(name=name)
        except IntegrityError:
            return JsonResponse({"ERROR": f"Name '{name}' already exists"}, safe=False)

        song_id = request.POST.get("song", None)
        if song_id:
            try:
                song = Song.objects.get(id=song_id)
            except Song.DoesNotExist:
                return JsonResponse({"ERROR": f"Song id '{song_id}' does not exist"}, safe=False)
            song.artists.add(artist)

        return JsonResponse({"Artist Created": artist.id}, safe=False)

class ArtistDetailView(View):
    def get(self, request, *args, **kwargs):
        id = kwargs["id"]
        try:
            result = Artist.objects.values("id", "name").get(id=id)
        except Artist.DoesNotExist:
            return JsonResponse({"Artist does not exist": id}, safe=False)

        return JsonResponse(result, safe=False)