from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name="index"),
    path('users/', views.UserView.as_view(), name="rasapi_user"),
    path('users/<uuid:id>/', views.UserDetailView.as_view(), name="rasapi_user_detail"),

    path('songs/', views.SongView.as_view(), name="rasapi_song"),
    path('songs/<int:id>/', views.SongDetailView.as_view(), name="rasapi_song_detail"),

    path('sources/', views.SourceView.as_view(), name="rasapi_source"),
    path('sources/<int:id>/', views.SourceDetailView.as_view(), name="rasapi_source_detail"),

    path('genres/', views.GenreView.as_view(), name="rasapi_genres"),
    path('genres/<int:id>/', views.GenreDetailView.as_view(), name="rasapi_genre_detail"),

    path('artists/', views.ArtistView.as_view(), name="rasapi_artist"),
    path('artists/<int:id>/', views.ArtistDetailView.as_view(), name="rasapi_artist_detail"),
]
