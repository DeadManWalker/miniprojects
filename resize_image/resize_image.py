#!/usr/bin/python3

"""
Call:
'resize_image.py -h'
for help
"""

import os
import sys
import argparse
from PIL import Image


parser = argparse.ArgumentParser()
parser.add_argument("-p", "--path", help="Image directory", required=True)
parser.add_argument("-ww", "--width", help="Image width", type=int, required=True)
parser.add_argument("-hh", "--height", help="Image height", type=int, default=0)
parser.add_argument("-s", "--suffix", help="Suffix", default="_resize")
parser.add_argument("-d", "--dest", help="Destination", default=None)
args = parser.parse_args()
args.height = args.height if args.height != 0 else args.width
args.path = os.path.normpath(args.path)
args.dest = args.dest if args.dest is not None else args.path

print("Resizing images in path '%s' to size %sx%s" %(args.path, args.width, args.height))
print("")
for _imfile in os.listdir(args.path):
    imfile = os.path.join(args.path, _imfile)
    path_no_ext = os.path.splitext(imfile)[0]
    try:
        im = Image.open(imfile)
        im.thumbnail((args.width, args.height), Image.ANTIALIAS)
        im.save(path_no_ext + args.suffix + ".jpeg", "JPEG")
    except IOError as e:
        print("failed on image: '%s' with '%s'" %(imfile, e))
    else:
        print("Resized " + _imfile)

print("")
print("...Done")
print("")
